FROM php:7.4-apache
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN usermod -u 1000 www-data
RUN a2enmod rewrite
RUN apt-get update \
    && apt-get install -y zlib1g-dev libzip-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install zip 
