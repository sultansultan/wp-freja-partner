#!/bin/bash
err=0
trap 'err=1' ERR

docker-compose exec -T web sh -c "./wp-content/vendor/bin/phpcbf wp-content/themes/custom-theme/"
docker-compose exec -T web sh -c "./wp-content/vendor/bin/phpcs wp-content/themes/custom-theme/"

cd frontend
npm run build

test $err = 0
