var fs = require('fs-extra');

var source = './dist';
var dest = '../wp-content/themes/custom-theme/dist';
fs.copy(source, dest, (err) => {
	console.log('Running release to theme..')
	if (err)
	{
		return console.error(err);
	}
	console.log('Dist copied to theme');
});