const path = require("path");
// const HtmlWebpackPlugin = require("html-webpack-plugin");
const globImporter = require("node-sass-glob-importer");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const WebpackShellPlugin = require("webpack-shell-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");

const paths = {
	themeRoot: "../wp-content/themes/custom-theme"
};

module.exports = {
	entry: {
		main: "./src/index.js"
	},
	mode: "development",
	output: {
		filename: "[name]-bundle.js",
		path: path.resolve(__dirname, "dist"),
		publicPath: "/"
	},
	devServer: {
		contentBase: path.join(__dirname, "dist"),
		compress: true,
		port: 8082,
		proxy: {
			"/": "http://localhost:8080"
		},
		watchContentBase: true,
		stats: {
			children: false, // Hide children information
			maxModules: 0 // Set the maximum number of modules to be shown
		},
		headers: {
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
			"Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
		}
	},
	module: {
		rules: [
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader"
						//options: { minimize: true }
					}
				]
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ["babel-loader", "eslint-loader"]
			},
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							// you can specify a publicPath here
							// by default it uses publicPath in webpackOptions.output
							publicPath: "../",
							hmr: process.env.NODE_ENV === "development"
						}
					},
					{
						// This loader resolves url() and @imports inside CSS
						loader: "css-loader"
					},
					{
						// First we transform SASS to standard CSS
						loader: "sass-loader",
						options: {
							implementation: require("sass"),
							sassOptions: {
								importer: globImporter(),
								includePaths: ["node_modules/foundation-sites/scss"]
							}
						}
					}
				]
			}
		]
	},
	resolve: {
		alias: {
			styles: path.resolve(__dirname, "src", "scss")
		}
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "css/styles.css"
		}),
		new CopyWebpackPlugin([
			{ from: "src/fonts", to: "fonts" },
			{ from: "src/icons", to: "icons" },
			{ from: "src/img", to: "img" }
			//{ from: 'dist', to: '../wp-content/themes/custom-theme/dist'}
		]),
		new WebpackShellPlugin({ onBuildEnd: ["node releaseToTheme.js"] }),
		new BrowserSyncPlugin(
			{
				proxy: "http://localhost:8082",
				port: 3000,
				host: "localhost",
				reloadDelay: 0,
				files: [`${paths.themeRoot}/**/*.php`]
			},
			{
				reload: false,
				injectCss: true
			}
		)
	]
};
