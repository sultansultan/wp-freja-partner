import 'foundation';
import 'styles/main.scss';
import './js/quote-slider';
import './js/latest-news-slider';
import './js/latest-case-slider';
import './js/packages-slider';
import './js/youtube-block';
import './js/show-more';