(function ($) {
	/**
     * initializeBlock
     *
     * Adds custom JavaScript to the block HTML.
     *
     * @date    15/4/19
     * @since   1.0.0
     *
     * @param   object $block The block jQuery element.
     * @param   object attributes The block attributes (only available when editing).
     * @return  void
     */
	var initializeBlock = function( $block ) {
		$block.find('.fp-quotes__slider').slick({
			centerMode: true,
			centerPadding: '60px',
			slidesToShow: 2,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			variableWidth: true,
			dots: false,
			arrows: false,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						arrows: false,
						centerMode: true,
						slidesToShow: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						arrows: false,
						centerMode: true,
						slidesToShow: 1
					}
				}
			]
		});
	}

	// Initialize each block on page load (front end).
	$(document).ready(function(){
		$('.fp-quotes__slider-block').each(function(){
			initializeBlock( $(this) );
		});
	});

	// Initialize dynamic block preview (editor).
	if( window.acf ) {
		window.acf.addAction( 'render_block_preview/type=slider', initializeBlock );
	}
})(jQuery);