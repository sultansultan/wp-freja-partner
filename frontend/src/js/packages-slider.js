(function ($) {
	$(function () {
		$().slick && $('.fp-packages__wrapper--mobile').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
  			centerMode: true,
			infinite: false,
			arrows: false,
			initialSlide: 1,
		})
	});
})(jQuery);