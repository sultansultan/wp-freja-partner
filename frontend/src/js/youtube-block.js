( function() {
	let container = document.getElementById('fp-video');

	let button = document.getElementById('fp-play');

	let background = document.querySelector('.fp-video__background');

	let yvideo = document.getElementById('yvideo');

	if(button != null) {
		button.onclick = function () {
			if (-1 !== container.className.indexOf( 'playing' )) {
				container.classList.remove('playing');
			} else {
				container.classList.add('playing');
			}
		}
	}

	if(background != null) {
		background.addEventListener('click', () => {
			container.classList.remove('playing');
			yvideo.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
		})
	}
}) ();