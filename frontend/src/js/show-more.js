( function() {
	if(document.querySelector('#show-more-box')) {
		let container = document.querySelector('#show-more-box');

		let button = container.querySelector('#show-fields');
	
		button.addEventListener('click', () => {
			let showMore = container.querySelector('#show-more');
	
			if(showMore.classList.contains('show-more')) {
				showMore.classList.remove('show-more');
			} else {
				showMore.classList.add('show-more');
			}
		});
	}
}) ();