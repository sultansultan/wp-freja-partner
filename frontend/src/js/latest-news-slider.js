(function ($) {
	$(function () {
		$().slick && $('.fp-latest-news__wrapper--mobile').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
  			centerMode: true,
			infinite: false,
			arrows: false,
		})
	});
})(jQuery);