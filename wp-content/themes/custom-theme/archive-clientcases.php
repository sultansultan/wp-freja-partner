<?php
/**
 * The template for displaying archive page for client cases.
 *
 * @package custom-theme
 */

$custom_theme_banner_background_img   = get_field( 'banner_background_img' ) ?: get_field( 'banner_background_img', 'options' );// phpcs:ignore
$custom_theme_banner_background_title = get_field( 'banner_title' ) ?: get_field( 'banner_title', 'options' );// phpcs:ignore
$custom_theme_banner_background_text  = get_field( 'banner_text' ) ?: get_field( 'banner_text', 'options' );// phpcs:ignore

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main site-main--no-padding site-main--background-color">
		<?php
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<div class="breadcrumbs breadcrumbs--grey">', '</div>' );
		}
		?>

		<header class="fp-articles-header">
			<?php
			$custom_theme_banner = get_field( 'case_banner', 'options' );
			if ( $custom_theme_banner ) :
				?>
				<div class="fp-top-banner" style="background-image: url('<?php echo esc_attr( $custom_theme_banner['background'] ); ?>')">
					<div class="fp-top-banner__overlay" ></div>
					<h1 class="fp-top-banner__title text-white">
						<?php echo esc_html( $custom_theme_banner['title'] ); ?>
					</h1>
					<p class="fp-top-banner__text text-sm text-white">
						<?php echo esc_attr( $custom_theme_banner['text'] ); ?>
					</p>
				</div>
			<?php endif; ?>
		</header>

		<section class="fp-articles fp-customer-cases">
			<?php
			$custom_theme_loop = new WP_Query(
				array(
					'post_type'      => 'clientcases',
					'posts_per_page' => -1,
				)
			);
			?>

			<?php if ( $custom_theme_loop->have_posts() ) : ?>
				<?php
				while ( $custom_theme_loop->have_posts() ) :
					$custom_theme_loop->the_post();
					?>
					<article class="fp-post-article">
						<a class="fp-post-article--link" href="<?php echo esc_url( get_permalink() ); ?>">
							<div class="fp-post-article__img">
								<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail( 'medium' );
								} else {
									echo '<div class="fp-post-article__img--placeholder"></div>';
								}
								?>
							</div>
							<div class="fp-post-article__content">
								<p class="text-xs text-darker-grey-blue text-normal fp-customer-cases__category">
									Kundcase
								</p>

								<h2 class="text-bold fp-customer-cases__title">
									<?php the_field( 'quote' ); ?>
								</h2>

								<p class="text-xs text-dark-grey-blue text-normal fp-customer-cases__quote-holder">
									<?php the_field( 'name' ); ?>, <?php the_field( 'company' ); ?>
								</p>
							</div>
						</a>
					</article>
				<?php endwhile; ?>
				<?php
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>
		</section>
	</main><!-- #main -->
</div><!-- #primary -->
</div>
<?php
get_footer();
