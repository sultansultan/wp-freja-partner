<?php
/**
 * The template for displaying client cases single posts.
 *
 * @package custom-theme
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main site-main--no-padding fp-single-article--background">
			<?php
			if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb( '<div class="breadcrumbs breadcrumbs--grey">', '</div>' );
			}
			?>

			<article class="fp-single-article">
				<div class="fp-single-article__img" style="background-image: url('<?php echo esc_url( get_the_post_thumbnail_url() ); ?>')"></div>

				<div class="fp-single-article-content">
					<div class="fp-single-article-content__left">
						<h1>
							<?php the_field( 'company' ); ?>
						</h1>

						<p class="text-xs text-darker-grey-blue text-normal">
							Kundcase
						</p>

						<span class="fp-post-quote">
							<p class="fp-post-quote__quote">
								<?php the_field( 'quote' ); ?>
							</p>

							<?php if ( get_field( 'name' ) ) : ?>
								<p class="fp-post-quote__name">
									<?php the_field( 'name' ); ?>
								</p>
							<?php endif; ?>
						</span>
					</div>

					<div class="fp-single-article-content__right">
						<?php
						if ( get_field( 'ingress' ) ) :
							?>
							<p class="fp-single-article-content__ingress">
								<?php the_field( 'ingress' ); ?>
							</p>
							<?php
						endif;

						if ( get_field( 'text' ) ) :
							?>
							<div class="fp-single-article-content__main-text">
								<?php the_field( 'text' ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</article>

			<?php
			custom_theme_get_custom_block(
				'template-parts/blocks/latest-case/latest-case'
			);
			?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
