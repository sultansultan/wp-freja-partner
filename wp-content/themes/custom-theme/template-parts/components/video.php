<?php
/**
 * Template for video component.
 *
 * @package custom-theme
 */

if ( get_field( 'video' ) ) {
	$custom_theme_iframe = get_field( 'video' );
} else {
	$custom_theme_iframe = get_field( 'post_video', $custom_theme_queried_object );
}

// Use preg_match to find iframe src.
preg_match( '/src="(.+?)"/', $custom_theme_iframe, $matches );
$custom_theme_src = $matches[1] . '&enablejsapi=1';

$custom_theme_thumbnail = wp_sprintf( 'https://img.youtube.com/vi/%s/hqdefault.jpg', custom_theme_get_yt_id( $custom_theme_src ) );
?>
<div class="fp-video__video" id="fp-video">
	<div class="fp-video__youtube-thumbnail" style="background-image: url('<?php echo esc_attr( $custom_theme_thumbnail ); ?>')">
		<button class="fp-video__youtube-thumbnail__play" id="fp-play">
			<img src="/wp-content/themes/custom-theme/dist/icons/Play-icon.svg"/>
		</button>
	</div>

	<div class="fp-video__background">
		<div class="fp-video__pause-container">
			<button class="fp-video__pause" id="fp-pause">
				<img src="/wp-content/themes/custom-theme/dist/icons/close-icon.svg" alt="Stäng videospelaren"/>
			</button>
		</div>

		<iframe
			class="fp-video__iframe"
			id="yvideo"
			src="<?php echo esc_html( $custom_theme_src ); ?>"
			allow="fullscreen;"
		></iframe>
	</div>
</div>
