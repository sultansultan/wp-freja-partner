<?php
/**
 * Template part for displaying the single post
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package custom-theme
 */

?>

<?php
	$custom_theme_article_category       = get_the_category();
	$custom_theme_article_category_name  = $custom_theme_article_category[0]->name;
	$custom_theme_queried_object         = get_queried_object();
	$custom_theme_post_quote             = get_field( 'post-quote', $custom_theme_queried_object );
	$custom_theme_top_banner_curved_img  = get_field( 'top_banner_curved_img', $custom_theme_queried_object );
	$custom_theme_top_banner_curved_text = get_field( 'top_banner_curved_text', $custom_theme_queried_object );
?>

<article class="fp-single-article" id="article-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( $custom_theme_top_banner_curved_img ) : ?>
		<div class="fp-single-article__img" style="background-image: url('<?php echo esc_attr( $custom_theme_top_banner_curved_img ); ?>')">
			<?php if ( $custom_theme_top_banner_curved_text ) : ?>
				<h1 class="text-white"> <?php echo esc_attr( $custom_theme_top_banner_curved_text ); ?> </h1>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<div class="fp-single-article-content">
		<div class="fp-single-article-content__left">
			<?php the_title( '<h1>', '</h1>' ); ?>

			<p class="text-xs text-darker-grey-blue text-normal"><?php echo esc_attr( $custom_theme_article_category_name ); ?></p>

			<p class="text-xs text-dark-grey-blue text-normal"><?php echo esc_attr( get_the_date( 'd F Y' ) ); ?></p>

			<?php if ( $custom_theme_post_quote ) : ?>
				<span class="fp-post-quote">
					<p class="fp-post-quote__quote">
						<?php echo esc_attr( $custom_theme_post_quote ); ?>
					</p>
				</span>
			<?php endif; ?>
		</div>
		<div class="fp-single-article-content__right">
			<div class="fp-single-article-content__main-text">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
	custom_theme_get_custom_block(
		'template-parts/blocks/latest-news/latest-news'
	);
	?>
