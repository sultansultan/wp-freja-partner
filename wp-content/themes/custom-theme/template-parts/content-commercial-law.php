<?php
/**
 * Template for Commercial Law Competence page.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-com-law-competence';

?>

<?php
	$custom_theme_terms = get_the_terms( $post->ID, 'field' );
	$custom_theme_field = '';
foreach ( $custom_theme_terms as $custom_theme_term ) {
	$custom_theme_field = $custom_theme_term->slug;
}

	$custom_theme_args = array(
		'post_type'      => 'commercial-law',
		'posts_per_page' => -1,
		'tax_query' => array( // phpcs:ignore
			array(
				'taxonomy' => 'field',
				'field'    => 'slug',
				'terms'    => $custom_theme_field,
			),
		),
	);

	$custom_theme_commercial_law_field_posts = get_posts( $custom_theme_args );
	?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<?php if ( get_the_post_thumbnail_url() ) : ?>
		<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__feat" 
			style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url() ); ?>)"
		></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__content">
		<article class="<?php echo esc_attr( $custom_theme_class_name ); ?>__content-left">
			<?php the_title( '<h1 class="fp-com-law-competence__content-title">', '</h1>' ); ?>

			<?php the_content(); ?>

			<?php if ( $custom_theme_commercial_law_field_posts ) : ?>
				<div>
					<ul class="fp-single-commercial-law__posts">
						<?php
						foreach ( $custom_theme_commercial_law_field_posts as $custom_theme_commercial_law_field_post ) :
							$custom_theme_commercial_law_field_post_permalink = get_permalink( $custom_theme_commercial_law_field_post->ID );
							$custom_theme_commercial_law_field_post_title     = get_the_title( $custom_theme_commercial_law_field_post->ID );
							?>
							<li class="fp-single-commercial-law__posts-item">
								<a class="fp-link" href="<?php echo esc_url( $custom_theme_commercial_law_field_post_permalink ); ?>">
									<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
									<span class="fp-link__text text-darker-blue"><?php echo esc_attr( $custom_theme_commercial_law_field_post_title ); ?></span>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
		</article>
		<aside class="<?php echo esc_attr( $custom_theme_class_name ); ?>__content-right">
			<?php
				custom_theme_get_custom_block(
					'template-parts/blocks/packages/package-showcase'
				);
				?>
		</aside>
	</div>
</div>

<?php
	custom_theme_get_custom_block(
		'template-parts/blocks/we-can/we-can'
	);
	?>
