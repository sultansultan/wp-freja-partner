<?php
/**
 * Template part for displaying the article card
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package custom-theme
 */

?>

<?php
	$custom_theme_article_category      = get_the_category();
	$custom_theme_article_category_name = $custom_theme_article_category[0]->name;
?>


<article class="fp-post-article">
	<a class="fp-post-article--link" href="<?php echo esc_url( get_permalink() ); ?>">
		<div class="fp-post-article__img">
			<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail( 'medium' );
			} else {
				echo '<div class="fp-post-article__img--placeholder"></div>';
			}
			?>
		</div>
		<div class="fp-post-article__content">
			<p class="text-xs text-darker-grey-blue text-normal"><?php echo esc_attr( $custom_theme_article_category_name ); ?></p>
			<?php
				the_title( '<h2 class="text-bold">', '</h2>' );
			?>
			<p class="text-xs text-dark-grey-blue text-normal"><?php echo esc_attr( get_the_date( 'd F Y' ) ); ?></p>
		</div>
	</a>
</article>
