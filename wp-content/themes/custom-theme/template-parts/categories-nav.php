<?php
/**
 * Template for categories navbar.
 *
 * @package custom-theme
 */

	$custom_theme_args                   = array(
		'taxonomy' => 'category',
		'orderby'  => 'name',
		'order'    => 'DESC',
	);
	$custom_theme_article_categories     = get_categories( $custom_theme_args );
	$custom_theme_article_category_class = ! $category_slug ? 'categories-nav--active-category' : '';
	?>


<ul class="categories-nav" >
	<?php
	$custom_theme_nav_categories = '<li><a href=/nyheter/'
	. " class='text-xs text-dark-grey-blue text-normal "
	. esc_html( $custom_theme_article_category_class )
	. "'>"
	. esc_attr__( 'Alla', 'custom-theme' )
	. '</a></li>';
	foreach ( $custom_theme_article_categories as $custom_theme_article_category ) {
		$custom_theme_article_category_class = '';
		if ( $category_slug === $custom_theme_article_category->slug ) {
			$custom_theme_article_category_class .= 'categories-nav--active-category';
		}
		$custom_theme_nav_category    = '<li><a href=/category/'
			. esc_html( $custom_theme_article_category->slug )
			. " class='text-xs text-dark-grey-blue text-normal "
			. esc_html( $custom_theme_article_category_class )
			. "'>"
			. esc_html( $custom_theme_article_category->name )
			. '</a></li>';
		$custom_theme_nav_categories .= $custom_theme_nav_category;
	}
	echo $custom_theme_nav_categories;// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	?>
</ul>
