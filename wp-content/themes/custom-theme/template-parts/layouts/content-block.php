<?php
// phpcs:ignoreFile -- Includes inline script, not tollerated by phpcs
/**
 * Template part for the builder
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package custom-theme
 */

/**
 * Generating blocks
 */
?>
<?php
if ( have_rows( 'content' ) ) {
	while ( have_rows( 'content' ) ) {
		the_row();
		if ( 'split_block_image_right' === get_row_layout() ) {
			?>
			<div class="custom-theme-block 
			<?php
			if ( get_sub_field( 'full_width' ) ) {
				echo ' full-width';}
			if ( get_sub_field( 'dynamic_height' ) ) {
				echo ' dynamic-height';}	
			?>
			">
				<div class="custom-theme-block__half"><?php the_sub_field( 'text' ); ?> </div>
				<div class="custom-theme-block__half image" style="background-image: url('<?php the_sub_field( 'image' ); ?>');"></div>
			</div>
			<?php
		}
		if ( 'split_block_image_left' === get_row_layout() ) {
			?>
			<div class="custom-theme-block image-left 
			<?php
			if ( get_sub_field( 'full_width' ) ) {
				echo ' full-width';}
			if ( get_sub_field( 'dynamic_height' ) ) {
				echo ' dynamic-height';}
			?>
			">
				<div class="custom-theme-block__half image" style="background-image: url('<?php the_sub_field( 'image' ); ?>');"></div>
				<div class="custom-theme-block__half"><?php the_sub_field( 'text' ); ?> </div>
			</div>
			<?php
		}
		if ( 'split_block_map' === get_row_layout() ) {
			$custom_theme_coords = get_sub_field( 'adress' );

			?>
			<div class="custom-theme-block
			<?php
			if ( get_sub_field( 'full_width' ) ) {
				echo ' full-width';}
			?>
			">
			<div class="custom-theme-block__half"><?php the_sub_field( 'text' ); ?> </div>
			<div class="custom-theme-block__half image custom-theme-map"></div>
			</div>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHjHYqIkuTuFYbgoM_hc2BIfTjOoxsbZw"></script>
			<script type="text/javascript">
			(function($) {

			function new_map( $el ) {
				var $markers = $el.find('.marker');
				var args = {
					zoom		: 16,
					center		: new google.maps.LatLng(<?php echo esc_attr( $custom_theme_coords['lat'], 'custom-theme' ); ?>, <?php echo esc_attr( $custom_theme_coords['lng'], 'custom-theme' ); ?>),
					mapTypeId	: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map( $el[0], args);
				map.markers = [];
				$markers.each(function(){
					add_marker( $(this), map );
				});
				center_map( map );
				return map;
			}

			function add_marker( $marker, map ) {

				// var
				var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

				// create marker
				var marker = new google.maps.Marker({
					position	: latlng,
					map			: map
				});

				// add to array
				map.markers.push( marker );

				// if marker contains HTML, add it to an infoWindow
				if( $marker.html() )
				{
					// create info window
					var infowindow = new google.maps.InfoWindow({
						content		: $marker.html()
					});

					// show info window when marker is clicked
					google.maps.event.addListener(marker, 'click', function() {

						infowindow.open( map, marker );

					});
				}

			}


			function center_map( map ) {

				// vars
				var bounds = new google.maps.LatLngBounds();

				// loop through all markers and create bounds
				$.each( map.markers, function( i, marker ){

					var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

					bounds.extend( latlng );

				});

				// only 1 marker?
				if( map.markers.length == 1 )
				{
					// set center of map
					map.setCenter( bounds.getCenter() );
					map.setZoom( 16 );
				}
				else
				{
					// fit to bounds
					map.fitBounds( bounds );
				}

			}

			var map = null;

			$(document).ready(function(){

				$('.custom-theme-map').each(function(){

					// create map
					map = new_map( $(this) );

				});

			});

			})(jQuery);
			</script>


			<?php
		}
		?>
		<?php
	} /* Endwhile */
} /* Endif */
?>
