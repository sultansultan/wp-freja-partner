<?php
/**
 * Template for gutenberg block for text beside max. 3 cards.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-text-beside-cards';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="main-wrapper main-wrapper--off-white">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
		<article class="<?php echo esc_attr( $custom_theme_class_name ); ?>__main">
			<h2> <?php the_field( 'title' ); ?> </h2>
			<p> <?php the_field( 'text' ); ?> </p>

			<?php
			$custom_theme_link        = get_field( 'link' );
			$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
			?>
			<a
				class="<?php echo esc_attr( $custom_theme_class_name ); ?>__main-link fp-link"
				href="<?php echo esc_url( $custom_theme_link['url'] ); ?>"
				target="<?php echo esc_attr( $custom_theme_link_target ); ?>"
			>
					<span class="fp-link__text">
						<?php echo esc_html( $custom_theme_link['title'] ); ?>
					</span>

				<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
			</a>
		</article>

		<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__cards">
			<?php
			if ( have_rows( 'cards' ) ) :
				while ( have_rows( 'cards' ) ) :
					the_row();
					?>
					<article class="<?php echo esc_attr( $custom_theme_class_name ); ?>__card">
						<?php
						$custom_theme_card_link        = get_sub_field( 'link' );
						$custom_theme_card_link_target = $custom_theme_card_link['target'] ? $custom_theme_card_link['target'] : '_self';
						?>
						<a
							class="<?php echo esc_attr( $custom_theme_class_name ); ?>__card-link fp-link"
							href="<?php echo esc_url( $custom_theme_card_link['url'] ); ?>"
							target="<?php echo esc_attr( $custom_theme_card_link_target ); ?>"
						>
							<span>
								<?php
								$custom_theme_icon = get_sub_field( 'icon' );
								if ( $custom_theme_icon ) :
									?>
									<img
										class="<?php echo esc_attr( $custom_theme_class_name ); ?>__card-icon"
										src="<?php echo esc_url( $custom_theme_icon['url'] ); ?>"
										alt="<?php echo esc_attr( $custom_theme_icon['alt'] ); ?>"
									/>
								<?php endif; ?>

								<h3 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__card-title">
									<?php the_sub_field( 'title' ); ?>
								</h3>

								<p class="<?php echo esc_attr( $custom_theme_class_name ); ?>__card-text">
									<?php the_sub_field( 'text' ); ?>
								</p>
							</span>

							<img class="fp-link__arrow" src="/frontend/src/icons/Arrow-icon.svg"/>
						</a>
					</article>
					<?php
				endwhile;
			endif;
			?>
		</div>
	</div>
</div>

