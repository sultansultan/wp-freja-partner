<?php
/**
 * Template for gutenberg block hero.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-text-content';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__wrapper">
		<h2> 
			<?php
				the_field( 'title' );
			?>
		</h2>
		<p>
			<?php
				the_field( 'column_text' );
			?>
		</p>
	</div>
</div>
