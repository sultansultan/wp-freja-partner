<?php
/**
 * Template for gutenberg block for contact information (Contact Us).
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-coworkers';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container" id="medarbetare">
	<h2 class="fp-coworkers__title"><?php the_field( 'title' ); ?></h2>

	<div class="fp-coworkers__coworkers-container">
		<?php
		$custom_theme_coworkers = new WP_Query(
			array(
				'post_type' => 'coworkers',
				'nopaging'  => true,
			)
		);

		while ( $custom_theme_coworkers->have_posts() ) :
			$custom_theme_coworkers->the_post();
			?>
			<div class="fp-coworkers__coworker">
				<div class="fp-coworkers__coworker-img-container">
					<img class="fp-coworkers__coworker-img" src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID() ) ); ?>">
				</div>
				<div class="fp-coworkers__coworker-text">
					<p class="fp-coworkers__coworker-name"><?php the_title(); ?></p>
					<p class="fp-coworkers__coworker-type"><?php echo esc_html( get_field( 'type', get_the_ID() ) ); ?></p>
					<a
						href="mailto:<?php echo esc_html( get_field( 'email', get_the_ID() ) ); ?>"
						class="fp-coworkers__coworker-email"
					>
						<?php the_field( 'email', get_the_ID() ); ?>
					</a>
					<?php if ( get_field( 'phone', get_the_ID() ) ) : ?>
						<a
							href="tel:<?php echo esc_html( get_field( 'phone', get_the_ID() ) ); ?>"
							class="fp-coworkers__coworker-phone"
						>
							<?php the_field( 'phone', get_the_ID() ); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</div>
