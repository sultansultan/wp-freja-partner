<?php
/**
 * Template for gutenberg block for contact information (Contact Us).
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-contact-form';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
	<div class="fp-contact-form__form">
		<p class="fp-contact-form__form-text"><?php the_field( 'text' ); ?></p>
		<?php the_field( 'contactform' ); ?>
	</div>

	<div class="fp-contact-form__ask-law">
		<?php if ( have_rows( 'ask_law' ) ) : ?>
			<?php
			while ( have_rows( 'ask_law' ) ) :
				the_row();
				?>
					<div class="fp-contact-form__ask-law-img" style="background-image: url('<?php echo esc_attr( get_sub_field( 'image' ) ); ?>')"></div>

					<section class="fp-contact-form__ask-law-content">
						<h2 class="fp-contact-form__ask-law-content-title">
							<?php the_sub_field( 'title' ); ?>
						</h2>

						<span class="fp-contact-form__ask-law-content-text">
							<?php the_sub_field( 'short_text' ); ?>
						</span>

						<?php
						$custom_theme_link = get_sub_field( 'link' );

						if ( $custom_theme_link ) :
							$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
							?>
							<a class="fp-button fp-button--light fp-contact-form__ask-law-button" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
								<img class="fp-contact-form__ask-law-button-img" src="/frontend/src/icons/Chat-icon.svg"/>

								<span>
									<?php echo esc_html( $custom_theme_link['title'] ); ?>
								</span>
							</a>
						<?php endif; ?>
					</section>
				<?php
			endwhile;
		endif;
		?>
	</div>
</div>
