<?php
/**
 * Template for gutenberg block for contact information (Contact Us).
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-contact';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
	<div class="fp-contact__main">
		<?php if ( have_rows( 'contact_info' ) ) : ?>
			<?php
			while ( have_rows( 'contact_info' ) ) :
				the_row();
				?>
				<section class="fp-contact__content">
					<h3><?php the_sub_field( 'title' ); ?></h3>
					<?php the_sub_field( 'text' ); ?>
				</section>
				<?php
			endwhile;
		endif;
		?>
	</div>
	<div class="fp-contact__socials">
		<?php the_field( 'social_media' ); ?>
	</div>
</div>
