<?php
/**
 * Template for gutenberg block hero.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-legalroom-content';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<?php
	if ( have_rows( 'content' ) ) :
		while ( have_rows( 'content' ) ) :
			the_row();
			$custom_theme_alignment = get_sub_field( 'left_right' ) ? 'left' : 'right';
			?>
		<article class="<?php echo esc_attr( $custom_theme_class_name ); ?>__article <?php echo esc_attr( $custom_theme_class_name ) . '__' . esc_attr( $custom_theme_alignment ); ?>">
			<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__image">
				<?php $custom_theme_image = get_sub_field( 'image' ); ?>
				<img src="<?php echo esc_attr( $custom_theme_image['url'] ); ?>" alt="<?php echo esc_attr( $custom_theme_image['alt'] ); ?>">
			</div>

			<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__content">
				<h2 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__content-title">
					<?php the_sub_field( 'title' ); ?>
				</h2>
				<p class="<?php echo esc_attr( $custom_theme_class_name ); ?>__content-text">
					<?php the_sub_field( 'text' ); ?>
				</p>
			</div>
		</article>
			<?php
	endwhile;
endif;
	?>
</div>
