<?php
/**
 * Template for gutenberg block for latest news.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-latest-case';

if ( is_front_page() ) {
	$custom_theme_class_name .= ' fp-latest-case--front';
}

if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="main-wrapper main-wrapper--off-white">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
		<section class="fp-articles fp-articles--mobile">
			<h2>Fler kundcase</h2>

			<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__wrapper <?php echo esc_attr( $custom_theme_class_name ); ?>__wrapper--desktop">
				<?php
				$custom_theme_loop = new WP_Query(
					array(
						'post_type'      => 'clientcases',
						'posts_per_page' => 3,
					)
				);
				?>

				<?php if ( $custom_theme_loop->have_posts() ) : ?>
					<?php
					while ( $custom_theme_loop->have_posts() ) :
						$custom_theme_loop->the_post();
						?>
						<article class="fp-post-article">
							<a class="fp-post-article--link" href="<?php echo esc_url( get_permalink() ); ?>">
								<div class="fp-post-article__img">
									<?php
									if ( has_post_thumbnail() ) {
										the_post_thumbnail( 'medium' );
									} else {
										echo '<div class="fp-post-article__img--placeholder"></div>';
									}
									?>
								</div>

								<div class="fp-post-article__content">
									<p class="text-xs text-darker-grey-blue text-normal <?php echo esc_attr( $custom_theme_class_name ); ?>__category">
										Kundcase
									</p>

									<h2 class="text-bold <?php echo esc_attr( $custom_theme_class_name ); ?>__title">
										<?php the_field( 'quote' ); ?>
									</h2>

									<p class="text-xs text-dark-grey-blue text-normal <?php echo esc_attr( $custom_theme_class_name ); ?>__quote-holder">
										<?php the_field( 'name' ); ?>, <?php the_field( 'company' ); ?>
									</p>
								</div>
							</a>
						</article>
						<?php
					endwhile;
				endif;
				?>
			</div>

			<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__wrapper <?php echo esc_attr( $custom_theme_class_name ); ?>__wrapper--mobile">
				<?php if ( $custom_theme_loop->have_posts() ) : ?>
					<?php
					while ( $custom_theme_loop->have_posts() ) :
						$custom_theme_loop->the_post();
						?>
						<article class="fp-post-article">
							<a class="fp-post-article--link" href="<?php echo esc_url( get_permalink() ); ?>">
								<div class="fp-post-article__img">
									<?php
									if ( has_post_thumbnail() ) {
										the_post_thumbnail( 'medium' );
									} else {
										echo '<div class="fp-post-article__img--placeholder"></div>';
									}
									?>
								</div>

								<div class="fp-post-article__content">
									<p class="text-xs text-darker-grey-blue text-normal <?php echo esc_attr( $custom_theme_class_name ); ?>__category">
										Kundcase
									</p>

									<h2 class="text-bold <?php echo esc_attr( $custom_theme_class_name ); ?>__title">
										<?php the_field( 'quote' ); ?>
									</h2>

									<p class="text-xs text-dark-grey-blue text-normal <?php echo esc_attr( $custom_theme_class_name ); ?>__quote-holder">
										<?php if ( get_field( 'name' ) ) : ?>
											<?php the_field( 'name' ); ?>,
										<?php endif; ?>
										<?php the_field( 'company' ); ?>
									</p>
								</div>
							</a>
						</article>
						<?php
					endwhile;
				endif;
				?>
			</div>

			<a class="fp-link" href="<?php echo esc_url( get_home_url( null, '/kundcase' ) ); ?>">
				<span class="fp-link__text">
					Se våra kundcase
				</span>

				<img class="fp-link__arrow" src="/frontend/src/icons/Arrow-icon.svg"/>
			</a>
		</section>
	</div>
</div>
