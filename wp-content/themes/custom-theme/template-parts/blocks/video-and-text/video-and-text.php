<?php
/**
 * Template for gutenberg block: "Framtidens juristbyrå".
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-video-text';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}

// Side selector.
$custom_theme_side = 'left';

if ( get_field( 'left_right' ) ) {
	$custom_theme_side = 'right';
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-video-text__<?php echo esc_attr( $custom_theme_side ); ?> fp-duo fp-duo--smaller-rows">
	<div class="fp-duo__left fp-duo__left__video">
		<?php
		if ( get_field( 'video' ) ) :
			require get_template_directory() . '/template-parts/components/video.php';
		endif;
		?>
	</div>

	<div class="fp-duo__right fp-duo__right__text fp-duo--text-center">
		<div class="fp-duo__text-container">
			<h1 class="fp-video-text__title">
				<?php the_field( 'title' ); ?>
			</h1>

			<span>
				<?php the_field( 'textfield' ); ?>
			</span>

			<?php
			$custom_theme_link = get_field( 'link' );

			if ( $custom_theme_link ) :
				$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
				?>
				<a class="fp-link" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
					<?php if ( $custom_theme_link['title'] ) : ?>
						<span class="fp-link__text">
							<?php echo esc_html( $custom_theme_link['title'] ); ?>
						</span>
					<?php endif; ?>

					<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
				</a>
			<?php endif; ?>
		</div>
	</div>
</div>
