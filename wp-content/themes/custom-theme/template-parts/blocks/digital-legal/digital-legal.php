<?php
/**
 * Template for gutenberg block "Digitala juristtjänster".
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-digital-legal';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="main-wrapper main-wrapper--yellow">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-duo fp-duo--auto-rows">
		<div class="fp-duo__left fp-duo__left__text fp-duo--text-center fp-duo--text-left">
			<div class="fp-duo__text-container">
				<h2 class="fp-digital-legal__title">
					<?php the_field( 'title' ); ?>
				</h2>

				<span class="bigger-text">
					<?php the_field( 'textfield' ); ?>
				</span>

				<?php
				$custom_theme_link = get_field( 'link' );

				if ( $custom_theme_link ) :
					$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
					?>
					<a class="fp-link" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
							<span class="fp-link__text">
								<?php echo esc_html( $custom_theme_link['title'] ); ?>
							</span>

						<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
					</a>
				<?php endif; ?>
			</div>
		</div>

		<div class="fp-duo__right fp-digital-legal__links">
			<div class="fp-digital-legal__links-wrapper">
				<?php if ( have_rows( 'page_linking' ) ) : ?>
					<?php
					while ( have_rows( 'page_linking' ) ) :
						the_row();
						?>
						<a class="fp-digital-legal__blog-links" href="<?php the_sub_field( 'link' ); ?>">
							<h3 class="fp-digital-legal__blog-title"><?php the_sub_field( 'title' ); ?></h3>
							<p class="fp-digital-legal__blog-text"><?php the_sub_field( 'textfield' ); ?></p>
						</a>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
