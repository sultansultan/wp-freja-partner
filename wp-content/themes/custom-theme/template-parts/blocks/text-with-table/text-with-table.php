<?php
/**
 * Template for gutenberg block for text with cards.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-text-with-table';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__max-width fp-container"> 
		<h1 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__title"> <?php the_field( 'title' ); ?> </h1>

		<?php
		if ( get_field( 'text_before' ) ) :
			the_field( 'text_before' );
		endif;
		?>

		<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__table-container">
			<table class="<?php echo esc_attr( $custom_theme_class_name ); ?>__table">
				<?php
				if ( have_rows( 'table' ) ) :
					while ( have_rows( 'table' ) ) :
						the_row();

						if ( get_sub_field( 'is_heading' ) ) :
							if ( have_rows( 'row' ) ) :
								?>
								<tr>
									<?php
									while ( have_rows( 'row' ) ) :
										the_row();
										?>
											<th class="<?php echo esc_attr( $custom_theme_class_name ); ?>__table-heading">
												<?php the_sub_field( 'cell' ); ?>
											</th>
									<?php endwhile; ?>
								</tr>
								<?php
							endif;
							else :
								if ( have_rows( 'row' ) ) :
									?>
								<tr>
										<?php
										while ( have_rows( 'row' ) ) :
											the_row();
											?>
											<td class="<?php echo esc_attr( $custom_theme_class_name ); ?>__table-cell">
												<?php the_sub_field( 'cell' ); ?>
											</td>
										<?php endwhile; ?>
								</tr>
									<?php
							endif;
						endif;
					endwhile;
				endif;
				?>
			</table>
		</div>

		<?php
		if ( get_field( 'text_after' ) ) :
			the_field( 'text_after' );
		endif;
		?>

		<p class="<?php echo esc_attr( $custom_theme_class_name ); ?>__date">
			Senast uppdaterad <?php echo esc_html( get_the_modified_time( 'j F Y' ) ); ?>
		</p>
	</div>
</div>
