<?php
/**
 * Template for gutenberg block for image and text surrounding it.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-showcase';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}

if ( get_field( 'video' ) ) {
	// Load value.
	$custom_theme_iframe = get_field( 'video' );

	// Use preg_match to find iframe src.
	preg_match( '/src="(.+?)"/', $custom_theme_iframe, $custom_theme_matches );
	$custom_theme_src = $custom_theme_matches[1];

	// Get video ID.
	$custom_theme_video       = wp_parse_url( $custom_theme_src );
	$custom_theme_video_embed = explode( '/', $custom_theme_video['path'] );
	$custom_theme_video_id    = $custom_theme_video_embed[2];

	// Add extra parameters to src and replcae HTML.
	$custom_theme_params = array(
		'autoplay'       => 1,
		'controls'       => 0,
		'playsinline'    => 1,
		'mute'           => 1,
		'loop'           => 1,
		'modestbranding' => 1,
		'playlist'       => $custom_theme_video_id,
	);

	$custom_theme_new_src = add_query_arg( $custom_theme_params, $custom_theme_src );

	// Add extra attributes to iframe HTML.
	$custom_theme_attributes = 'frameborder="0" allowfullscreen="1" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"';
}
?>

<div class="main-wrapper">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
		<div class="fp-showcase__intro">
			<p class="fp-showcase__intro-small-text"> <?php the_field( 'small_text' ); ?> </p>
			<h2> <?php the_field( 'title' ); ?> </h2>
			<p class="fp-showcase__intro-text"> <?php the_field( 'text' ); ?> </p>
		</div>
		<div class="fp-showcase__main">
			<?php if ( get_field( 'video' ) ) : ?>
				<div class="fp-showcase__main-video">
					<iframe src="<?php echo esc_url( $custom_theme_new_src ); ?>" <?php echo esc_attr( $custom_theme_attributes ); ?>></iframe>
				</div>
			<?php endif; ?>

			<?php
			if ( get_field( 'image' ) ) :
				$custom_theme_image = get_field( 'image' );
				?>
				<img class="fp-showcase__main-image" src="<?php echo esc_url( $custom_theme_image['url'] ); ?>" alt="<?php echo esc_attr( $custom_theme_image['alt'] ); ?>" />
			<?php endif; ?>

			<?php
			if ( have_rows( 'texts' ) ) :
				while ( have_rows( 'texts' ) ) :
					the_row();
					?>
					<section class="fp-showcase__content">
						<h3 class="fp-showcase__content-title"> <?php the_sub_field( 'title' ); ?> </h3>
						<p class="fp-showcase__content-text"> <?php the_sub_field( 'text' ); ?>  </p>
					</section>
					<?php
				endwhile;
			endif;
			?>
		</div>
		<div class="fp-showcase__nav">
			<?php
			$custom_theme_button            = get_field( 'button' );
				$custom_theme_button_target = $custom_theme_button['target'] ? $custom_theme_button['target'] : '_self';
			?>
			<a class="fp-button fp-button--orange fp-button--fixed-width" href="<?php echo esc_url( $custom_theme_button['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_button_target ); ?>">
				<?php echo esc_html( $custom_theme_button['title'] ); ?>
			</a>

			<?php
			$custom_theme_link            = get_field( 'link' );
				$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
			?>
			<a class="fp-showcase__nav-link" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
				<?php echo esc_html( $custom_theme_link['title'] ); ?>
			</a>
		</div>
	</div>
</div>
