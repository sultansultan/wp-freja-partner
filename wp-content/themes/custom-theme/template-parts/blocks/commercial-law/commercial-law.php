<?php
/**
 * Template for gutenberg block to show all fields of commercial law.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-commercial-law';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<?php
	$custom_theme_commercial_law_fields = get_field( 'commercial_law_order', 'option' );
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>--max-width">
	<section class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
		<?php if ( $custom_theme_commercial_law_fields ) : ?>
			<?php
			foreach ( $custom_theme_commercial_law_fields as $custom_theme_commercial_law_field ) :
				$custom_theme_commercial_law_field_name           = $custom_theme_commercial_law_field->name;
				$custom_theme_commercial_law_field_featured_image = get_field( 'commercial_law_featured_image', $custom_theme_commercial_law_field );
				$custom_theme_commercial_law_field_url            = get_term_link( $custom_theme_commercial_law_field );
				?>
				<article class="fp-commercial-law-field" style="background-image: url('<?php echo esc_attr( $custom_theme_commercial_law_field_featured_image ); ?>')">
					<a class="fp-commercial-law-field--link" href="<?php echo esc_url( $custom_theme_commercial_law_field_url ); ?>">
						<div class="fp-commercial-law-field__content">
							<h3 class="text-white text-bold"><?php echo esc_html( $custom_theme_commercial_law_field_name ); ?></h3>
							<img src="/frontend/src/icons/Arrow-icon.svg"/>
						</div>
					</a>
				</article>
			<?php endforeach; ?>
		<?php endif; ?>
	</section>
</div>
