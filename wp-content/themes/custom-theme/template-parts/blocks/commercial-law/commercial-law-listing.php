<?php
/**
 * Template for gutenberg block to show a listing of commercial law taxonomy.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-commercial-law-listing';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}

$custom_theme_commercial_law_fields = get_field( 'commercial_law_order', 'option' );
?>

<article class="<?php echo esc_attr( $custom_theme_class_name ); ?>" id="show-more-box">
	<h3 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__title">Affärsjuridik</h3>

	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__fields" id="show-more">
		<?php foreach ( $custom_theme_commercial_law_fields as $custom_theme_commercial_law_field ) : ?>
			<a class="<?php echo esc_attr( $custom_theme_class_name ); ?>__fields-link fp-link" href="<?php echo esc_url( get_term_link( $custom_theme_commercial_law_field ) ); ?>">
				<?php echo esc_html( $custom_theme_commercial_law_field->name ); ?>
			</a>
		<?php endforeach; ?>
	</div>

	<button
		class="<?php echo esc_attr( $custom_theme_class_name ); ?>__show-button"
		id="show-fields"
		type="button"
	>
		Visa alla områden <span class="<?php echo esc_attr( $custom_theme_class_name ); ?>__sign"> + </span>
	</button>
</article>
