<?php
/**
 * Template for gutenberg block to show top banner.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-buttons';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<?php
	if ( have_rows( 'buttons' ) ) :
		while ( have_rows( 'buttons' ) ) :
			the_row();
			?>
			<?php
			$custom_theme_button            = get_sub_field( 'button' );
				$custom_theme_button_target = $custom_theme_button['target'] ? $custom_theme_button['target'] : '_self';
			?>
				<a
					class="fp-button fp-button--fixed-width fp-button--<?php echo esc_attr( get_sub_field( 'color' ) ); ?>"
					href="<?php echo esc_url( $custom_theme_button['url'] ); ?>"
					target="<?php echo esc_attr( $custom_theme_button_target ); ?>"
				>
					<?php echo esc_html( $custom_theme_button['title'] ); ?>
				</a>
			<?php
		endwhile;
	endif;
	?>
</div>
