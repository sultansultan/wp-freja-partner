<?php
/**
 * Template for gutenberg block for packages solutions.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-packages';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="main-wrapper main-wrapper--off-white">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
		<h2 class="fp-packages__title">
			<?php
			if ( get_field( 'title' ) ) :
				the_field( 'title' );
			else :
				?>
				Våra paketlösningar
			<?php endif; ?>
		</h2>

		<p class="fp-packages__text">
			<?php
			if ( get_field( 'short_text' ) ) :
				the_field( 'short_text' );
			else :
				?>
				Affärsjuridiska tjänster till fast pris
			<?php endif; ?>
		</p>

		<div class="fp-packages__wrapper--desktop">
			<?php if ( have_rows( 'bundles', 'options' ) ) : ?>
				<?php
				while ( have_rows( 'bundles', 'options' ) ) :
					the_row();
					?>
					<article class="package">
						<div class="package__main">
							<h3 class="package__type"> <?php the_sub_field( 'name' ); ?> </h3>
							<p class="package__price"> <?php echo number_format( get_sub_field( 'price' ), 0, '.', ' ' ); ?> kr/mån </p>
							<p class="package__desc"> <?php the_sub_field( 'description' ); ?> </p>
						</div>

						<div>
							<div class="package__whats-init">
								<b>Vad ingår?</b>
								<?php if ( have_rows( 'whats_init' ) ) : ?>
									<ul>
										<?php
										while ( have_rows( 'whats_init' ) ) :
											the_row();
											?>
											<li> <?php the_sub_field( 'text' ); ?> </li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>
								<?php if ( have_rows( 'whats_not_init' ) ) : ?>
									<ul class="not-available">
										<?php
										while ( have_rows( 'whats_not_init' ) ) :
											the_row();
											?>
											<li> <?php the_sub_field( 'text' ); ?> </li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>
							</div>

							<a class="fp-button" href="<?php echo esc_url( get_sub_field( 'link' ) ); ?>">
								<span class="fp-button__text">
									Beställ
								</span>
							</a>

							<?php
							$custom_theme_small_link = get_sub_field( 'small_link' );

							if ( $custom_theme_small_link ) :
								$custom_theme_small_target = $custom_theme_small_link['target'] ? $custom_theme_small_link['target'] : '_self';
								?>
								<a class="fp-link fp-link--small fp-link--center" href="<?php echo esc_url( $custom_theme_small_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_small_target ); ?>">
									<span class="fp-link--small__text">
										<?php echo esc_html( $custom_theme_small_link['title'] ); ?>
									</span>
								</a>
							<?php endif; ?>
						</div>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>

		<div class="fp-packages__wrapper--mobile">
			<?php if ( have_rows( 'bundles', 'options' ) ) : ?>
				<?php
				while ( have_rows( 'bundles', 'options' ) ) :
					the_row();
					?>
					<article class="package">
						<div class="package__main">
							<h3 class="package__type"> <?php the_sub_field( 'name' ); ?> </h3>
							<p class="package__price"> <?php echo number_format( get_sub_field( 'price' ), 0, '.', ' ' ); ?> kr/mån </p>
							<p class="package__desc"> <?php the_sub_field( 'description' ); ?> </p>
						</div>

						<div>
							<div class="package__whats-init">
								<b>Vad ingår?</b>
								<?php if ( have_rows( 'whats_init' ) ) : ?>
									<ul>
										<?php
										while ( have_rows( 'whats_init' ) ) :
											the_row();
											?>
											<li> <?php the_sub_field( 'text' ); ?> </li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>
								<?php if ( have_rows( 'whats_not_init' ) ) : ?>
									<ul class="not-available">
										<?php
										while ( have_rows( 'whats_not_init' ) ) :
											the_row();
											?>
											<li> <?php the_sub_field( 'text' ); ?> </li>
										<?php endwhile; ?>
									</ul>
								<?php endif; ?>
							</div>

							<a class="fp-button" href="<?php echo esc_url( get_sub_field( 'link' ) ); ?>">
								<span class="fp-button__text">
									Beställ
								</span>
							</a>

							<?php
							$custom_theme_small_link = get_sub_field( 'small_link' );

							if ( $custom_theme_small_link ) :
								$custom_theme_small_target = $custom_theme_small_link['target'] ? $custom_theme_small_link['target'] : '_self';
								?>
								<a class="fp-link fp-link--small fp-link--center" href="<?php echo esc_url( $custom_theme_small_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_small_target ); ?>">
									<span class="fp-link--small__text">
										<?php echo esc_html( $custom_theme_small_link['title'] ); ?>
									</span>
								</a>
							<?php endif; ?>
						</div>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
