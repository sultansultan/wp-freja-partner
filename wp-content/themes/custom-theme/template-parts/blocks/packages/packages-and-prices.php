<?php
/**
 * Template for gutenberg block for packages solutions.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-packages-and-prices';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="main-wrapper main-wrapper--off-white">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
		<h1 class="fp-packages-and-prices__title"><?php the_field( 'title' ); ?></h1>
		<p class="fp-packages-and-prices__text"><?php the_field( 'short_text' ); ?></p>
		<div class="fp-packages-and-prices__bundles-container">
			<?php
			$custom_theme_rows = get_field( 'bundles', 'options' );
			if ( $custom_theme_rows ) :
				$custom_theme_amount_rows = count( $custom_theme_rows );
				?>
					<table class="fp-packages-and-prices__bundles">
						<tr>
							<th></th>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<th class="fp-packages-and-prices__bundles-name"> <?php echo esc_html( $custom_theme_rows[ $custom_theme_i ]['name'] ); ?> </th>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Juridisk hjälp</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td> <?php echo esc_html( $custom_theme_rows[ $custom_theme_i ]['hours'] ); ?> timmar/år </td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Juridisk rådgivning</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td><img class="fp-packages-and-prices__bundles-checkmark" src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/></td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Inkassohantering</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td><img class="fp-packages-and-prices__bundles-checkmark" src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/></td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Kreditupplysning</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td> <?php echo esc_html( $custom_theme_rows[ $custom_theme_i ]['credits'] ); ?> st </td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Föreläsningar och utbildning</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td><img class="fp-packages-and-prices__bundles-checkmark" src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/></td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Avtalsmallar</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td><img class="fp-packages-and-prices__bundles-checkmark" src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/></td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Checklistor</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td><img class="fp-packages-and-prices__bundles-checkmark" src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/></td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>Digital avtalssignering</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td>Tillval</td>
							<?php endfor; ?>
						</tr>
						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-choices">
							<td>eÄta</td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td>Tillval</td>
							<?php endfor; ?>
						</tr>

						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-prices">
							<td></td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td>
									<span class="fp-packages-and-prices__bundles-price">
										<?php echo number_format( $custom_theme_rows[ $custom_theme_i ]['price'], 0, '.', ' ' ); ?> kr/mån
									</span>
								</td>
							<?php endfor; ?>
						</tr>

						<tr class="<?php echo esc_attr( $custom_theme_class_name ); ?>__bundles-buttons">
							<td></td>
							<?php for ( $custom_theme_i = 0; $custom_theme_i < $custom_theme_amount_rows; $custom_theme_i++ ) : ?>
								<td>
									<a class="fp-button fp-button--fixed-width" href="<?php echo esc_url( $custom_theme_rows[ $custom_theme_i ]['link'] ); ?>">
										Beställ paket
									</a>
								</td>
							<?php endfor; ?>
						</tr>
					</table>
			<?php endif; ?>
		</div>
	</div>
</div>
