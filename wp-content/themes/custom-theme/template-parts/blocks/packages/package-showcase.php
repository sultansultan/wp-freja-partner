<?php
/**
 * Template for showcasing package info block.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-package-showcase';
?>

<article class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<span class="<?php echo esc_attr( $custom_theme_class_name ); ?>__content">
		<h3 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__title">Affärsjuridiska tjänster till fast pris</h3>

		<?php if ( have_rows( 'bundles', 'options' ) ) : ?>
			<?php
			while ( have_rows( 'bundles', 'options' ) ) :
				the_row();
				?>
					<p class="<?php echo esc_attr( $custom_theme_class_name ); ?>__pricing">
						Från <?php echo number_format( get_sub_field( 'price' ), 0, '.', ' ' ); ?> kr/mån
					</p>
									<span class="<?php echo esc_attr( $custom_theme_class_name ); ?>__list">
						<h4 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__list-title">Vad ingår?</h4>
						<?php if ( have_rows( 'whats_init' ) ) : ?>
							<ul>
								<?php
								while ( have_rows( 'whats_init' ) ) :
									the_row();
									?>
									<li> <?php the_sub_field( 'text' ); ?> </li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
						<?php if ( have_rows( 'whats_not_init' ) ) : ?>
							<ul class="not-available">
								<?php
								while ( have_rows( 'whats_not_init' ) ) :
									the_row();
									?>
									<li> <?php the_sub_field( 'text' ); ?> </li>
								<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</span>
				<?php
				break;
			endwhile;
			?>
		<?php endif; ?>
	</span>

	<a class="<?php echo esc_attr( $custom_theme_class_name ); ?>__button fp-button fp-button--fixed-width" href="<?php echo esc_url( get_field( 'bundles_link', 'options' ) ); ?>">
		Läs mer om våra paket
	</a>
</article>
