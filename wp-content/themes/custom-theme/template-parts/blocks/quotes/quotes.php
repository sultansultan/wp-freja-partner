<?php
/**
 * Template for gutenberg block to show quotes.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-quotes';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}

if ( $is_preview ) {
	$custom_theme_class_name .= ' is-admin';
}
?>

<div class="main-wrapper">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
		<div class="fp-quotes__slider-block">
			<div class="fp-quotes__slider">
			<?php
				$custom_theme_coworkers = new WP_Query(
					array(
						'post_type'      => 'clientcases',
						'posts_per_page' => -1,
					)
				);

				while ( $custom_theme_coworkers->have_posts() ) :
					$custom_theme_coworkers->the_post();

					if ( get_field( 'show', get_the_ID() ) ) :
						?>
						<a class="fp-quotes__slide" href="<?php the_permalink(); ?>">
							<?php if ( get_the_post_thumbnail_url( get_the_ID() ) ) : ?>
								<div class="fp-quotes__img">
									<img src="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID() ) ); ?>"/>
								</div>
							<?php endif; ?>

							<p class="fp-quotes__quote"> <?php the_field( 'quote', get_the_ID() ); ?> </p>
							<p class="fp-quotes__who"> <?php the_field( 'name', get_the_ID() ); ?>, <?php the_field( 'company', get_the_ID() ); ?> </p>
						</a>
						<?php
						endif;
				endwhile;
				?>
			</div>
		</div>

		<?php
		$custom_theme_link = get_field( 'quotes_link', 'options' );

		if ( $custom_theme_link ) :
			$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
			?>
			<a class="fp-link fp-link--center fp-quotes__link" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
				<span class="fp-link__text">
					<span class="fp-link__text">
						<?php echo esc_html( $custom_theme_link['title'] ); ?>
					</span>
				</span>

				<img class="fp-link__arrow fp-link__arrow--blue" src="/frontend/src/icons/Arrow-icon.svg"/>
			</a>
		<?php endif; ?>
	</div>
</div>
