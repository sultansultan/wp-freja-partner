<?php
/**
 * Template for gutenberg block for contact information (Contact Us).
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-digital-cta';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<h2 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__title">
		<?php the_field( 'cta_title' ); ?>
	</h2>

	<p class="<?php echo esc_attr( $custom_theme_class_name ); ?>__text">
		<?php the_field( 'cta_text' ); ?>
	</p>

	<?php
	$custom_theme_button = get_field( 'cta_button' );
	if ( $custom_theme_button ) :
		$custom_theme_button_target = $custom_theme_button['target'] ? $custom_theme_button['target'] : '_self';
		?>
		<a
			class="fp-button fp-button--orange fp-button--smaller"
			href="<?php echo esc_url( $custom_theme_button['url'] ); ?>"
			target="<?php echo esc_attr( $custom_theme_button_target ); ?>"
		>
			<?php echo esc_html( $custom_theme_button['title'] ); ?>
		</a>
	<?php endif; ?>

	<?php
	$custom_theme_link = get_field( 'cta_link' );
	if ( $custom_theme_link ) :
		$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
		?>
		<a
			class="<?php echo esc_attr( $custom_theme_class_name ); ?>__link"
			href="<?php echo esc_url( $custom_theme_link['url'] ); ?>"
			target="<?php echo esc_attr( $custom_theme_link_target ); ?>"
		>
			<?php echo esc_html( $custom_theme_link['title'] ); ?>
		</a>
	<?php endif; ?>
</div>
