<?php
/**
 * Template for gutenberg block for contact information (Contact Us).
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-just-text';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
	<h2 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__title">
		<?php the_field( 'title' ); ?>
	</h2>

	<?php the_field( 'text' ); ?>

	<p class="<?php echo esc_attr( $custom_theme_class_name ); ?>__date">
		Senast uppdaterad <?php echo esc_html( get_the_modified_time( 'j F Y' ) ); ?>
	</p>
</div>
