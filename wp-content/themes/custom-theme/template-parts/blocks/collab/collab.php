<?php
/**
 * Template for gutenberg block for text with cards.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-collab';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-container">
	<h3 class="<?php echo esc_attr( $custom_theme_class_name ); ?> __title">
		<?php the_field( 'collabtitle', 'options' ); ?>
	</h3>
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__partners">
		<?php
		if ( have_rows( 'collabpartners', 'options' ) ) :
			while ( have_rows( 'collabpartners', 'options' ) ) :
				the_row();
				$custom_theme_image = get_sub_field( 'image' );
				?>
				<?php
				$custom_theme_link = get_sub_field( 'link' );
				if ( $custom_theme_link ) :
					$custom_theme_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
					?>
					<a
						href="<?php echo esc_url( $custom_theme_link['url'] ); ?>"
						target="<?php echo esc_attr( $custom_theme_target ); ?>"
						class="<?php echo esc_attr( $custom_theme_class_name ); ?>__partners-logo"
					>
						<img src="<?php echo esc_url( $custom_theme_image['url'] ); ?>" alt="<?php echo esc_attr( $custom_theme_image['alt'] ); ?>" />
					</a>
				<?php else : ?>
					<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>__partners-logo">
						<img src="<?php echo esc_url( $custom_theme_image['url'] ); ?>" alt="<?php echo esc_attr( $custom_theme_image['alt'] ); ?>" />
					</div>
				<?php endif; ?>
				<?php
			endwhile;
		endif;
		?>
	</div>
</div>
