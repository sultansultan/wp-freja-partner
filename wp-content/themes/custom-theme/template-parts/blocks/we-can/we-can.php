<?php
/**
 * Template for gutenberg block hero.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-we-can';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<?php $custom_theme_content_id = 0; ?>
	<?php if ( have_rows( 'we_can', 'options' ) ) : ?>
		<?php
		while ( have_rows( 'we_can', 'options' ) ) :
			the_row();
			?>
			<?php $custom_theme_content_id++; ?>
			<div class="fp-we-can__content" style="background-image: url('<?php echo esc_attr( get_sub_field( 'image' ) ); ?>')">
				<div class="fp-we-can__overlay fp-we-can__overlay__<?php echo esc_attr( $custom_theme_content_id ); ?>"></div>
				<h2> <?php the_sub_field( 'title' ); ?> </h2>
				<p> <?php the_sub_field( 'short_text' ); ?> </p>
				<?php
				$custom_theme_link = get_sub_field( 'link' );

				if ( $custom_theme_link ) :
					$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
					?>
					<a class="fp-button fp-button--fixed-width fp-we-can__button__<?php echo esc_attr( $custom_theme_content_id ); ?>" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
						<span class="fp-button__text">
							<?php echo esc_html( $custom_theme_link['title'] ); ?>
						</span>
					</a>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
