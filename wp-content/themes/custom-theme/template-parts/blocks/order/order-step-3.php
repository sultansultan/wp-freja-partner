<?php
/**
 * File for section 3 in order-flow.
 *
 * @package custom-theme
 */

?>

<div class="fp-order-step-3">
	<h1>Tack för din beställning!</h1>
	<p>Vi har skickat en bekräftelse via mejl till <b>{{ customer.email }}</b>. Kontakta oss på info@frejapartner.se eller 031-000000 om du har några frågor.</p>

	<a
		class="fp-button fp-button--orange fp-button--fixed-width fp-order-step-3__button"
		href="#"
	>
		Logga in på LegalRoom
	</a>
</div>
