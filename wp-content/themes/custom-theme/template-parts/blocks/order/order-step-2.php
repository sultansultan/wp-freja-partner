<?php
/**
 * File for section 2 in order-flow.
 *
 * @package custom-theme
 */

?>

<div class="fp-order-step-2">
	<?php require get_template_directory() . '/template-parts/blocks/order/parts/header.php'; ?>

	<h2 class="fp-order-step-2__subheading">Tilläggstjänster</h2>
	<form class="fp-adding-form">
		<div>
			<button
				class="fp-adding-form__clear"
				:class="addedOptionalsID.length === 0 ? 'fp-adding-form__clear-set' : ''"
				type="button"
				@click="clearAddedOptionals"
			>
				Inga tillvals
			</button>
		</div>

		<div :key="optional.id" v-for="optional in optionals">
			<input
				class="fp-adding-form__checkbox"
				:id="'optional-' + optional.id"
				type="checkbox"
				:value="optional.id"
				v-model="addedOptionalsID"
			/>
			<label class="fp-adding-form__label" :for="'optional-' + optional.id">
				<span class="fp-adding-form__label-type">{{ optional.name }}</span>
				<span class="fp-adding-form__label-price">{{ thousandSeperator(optional.price) }} kr/mån</span>
				<a
					href="#"
					class="fp-adding-form__label-read"
					@click="togglePopup(optional.id)"
				>
					Läs mer
				</a>

				<?php require get_template_directory() . '/template-parts/blocks/order/parts/popup-read-more-optionals.php'; ?>
			</label>
		</div>
	</form>

	<?php require get_template_directory() . '/template-parts/blocks/order/parts/navigation.php'; ?>
</div>
