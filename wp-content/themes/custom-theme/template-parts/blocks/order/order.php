<?php
/**
 * Main file for order flow.
 *
 * @package custom-theme
 */

?>

<div class="main-wrapper main-wrapper--off-white">
	<div class="fp-order fp-container" id="fp-order">
		<?php require get_template_directory() . '/template-parts/blocks/order/parts/stepper.php'; ?>

		<div class="fp-order__container">
			<div v-if="step === 1" key="step-1">
				<?php require get_template_directory() . '/template-parts/blocks/order/order-step-1.php'; ?>
			</div>

			<div v-if="step == 2" key="step-2">
				<?php require get_template_directory() . '/template-parts/blocks/order/order-step-2.php'; ?>
			</div>

			<div v-if="step == 3" key="step-3">
				<?php require get_template_directory() . '/template-parts/blocks/order/order-step-3.php'; ?>
			</div>
		</div>

		<?php require get_template_directory() . '/template-parts/blocks/order/parts/order-info.php'; ?>
	</div>
</div>
