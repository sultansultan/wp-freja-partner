<?php
/**
 * Stepper for order flow.
 *
 * @package custom-theme
 */

?>

<div class="fp-stepper">
	<router-link
		:key="i"
		v-for="(step, i) in steps"
		class="fp-stepper__link"
		:class="isActive(step.step) ? 'fp-stepper__active' : '' "
		:to="'?step=' + step.step"
		@click.native.prevent="goToNext('?step=' + step.step)"
		event
	>
		{{ step.name }}
	</router-link>
</div>
