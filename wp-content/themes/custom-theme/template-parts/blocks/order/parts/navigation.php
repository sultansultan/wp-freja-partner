<?php
/**
 * Navigation between steps (not stepper).
 *
 * @package custom-theme
 */

?>

<div class="fp-order-navigation">
	<a
		v-if="step === 1"
		class="fp-order-navigation__back"
		href="/"
	>
		Tillbaka
	</a>

	<router-link
		class="fp-order-navigation__back"
		:to="'?step=' + previousStep"
		v-else
	>
		Tillbaka
	</router-link>

	<router-link
		class="fp-order-navigation__next fp-button fp-button--fixed-width fp-button--white-text"
		:to="'?step=' + nextStep"
		@click.native.prevent="sendOrder()"
		event
		v-if="step === steps.length - 1"
	>
		Slutför beställning
	</router-link>

	<router-link
		class="fp-order-navigation__next fp-button fp-button--fixed-width fp-button--white-text"
		:to="'?step=' + nextStep"
		@click.native.prevent="goToNext('?step=' + nextStep)"
		event
		v-else
	>
		Nästa steg
		<img class="fp-order-navigation__next-img" src="/wp-content/themes/custom-theme/dist/icons/Arrow-icon.svg"/>
	</router-link>
</div>
