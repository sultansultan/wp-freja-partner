<?php
/**
 * Popup (read more) for order flow.
 *
 * @package custom-theme
 */

?>

<div class="fp-popup-read-more">
	<section class="fp-popup-read-more__popup">
		<a
			href="#"
			class="fp-popup-read-more__close"
			@click="togglePopup(bundle.id)"
		>
			Stäng fönster <img class="fp-popup-read-more__close-img" src="/wp-content/themes/custom-theme/dist/icons/close-icon.svg">
		</a>

		<p> {{ bundle.description }} </p>

		<h2 class="fp-popup-read-more--boldify fp-popup-read-more__title">Vad ingår?</h2>
		<p class="fp-popup-read-more--no-margin-top"> {{ arrayToString(bundle.whats_init) }}. </p>

		<p class="fp-popup-read-more__popup-price">
			<span class="fp-popup-read-more--boldify">Pris:</span> {{ thousandSeperator(bundle.price) }} kr/mån
		</p>

		<a
			:href="bundle.small_link.url"
			class="fp-popup-read-more__popup-link"
		>
			Se alla våra paket
		</a>
	</section>
</div>
