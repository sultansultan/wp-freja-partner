<?php
/**
 * Stepper for order flow.
 *
 * @package custom-theme
 */

?>

<div class="fp-order-info">
	<h2 class="fp-order-info__title">Dina val</h2>
	<section class="fp-order-info__customer" v-if="customerReady">
		<h3>Beställare</h3>
		<p>
			{{ customer.company }} <br/>
			{{ customer.address }} <br/>
			{{ customer.zip }} {{ customer.place }}
		</p>
		<p>
			{{ customer.name }} {{ customer.surname }} <br/>
			{{ customer.email }}
		</p>
	</section>

	<p v-if="!loaded"> Laddar... </p>
	<section class="fp-order-info__bundle" v-else>
		<h3> {{ selectedBundle.name }}paketet</h3>
		<table>
			<tr>
				<td>Juridisk hjälp</td>
				<td> {{ selectedBundle.hours }} timmar/år</td>
			</tr>
			<tr>
				<td>Juridisk rådgivning</td>
				<td>
					<img src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/>
				</td>
			</tr>
			<tr>
				<td>Inkassohantering</td>
				<td>
					<img src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/>
				</td>
			</tr>
			<tr>
				<td>Kreditupplysning</td>
				<td> {{ selectedBundle.credits }} st</td>
			</tr>
			<tr>
				<td>Föreläsningar och utbildning</td>
				<td>
					<img src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/>
				</td>
			</tr>
			<tr>
				<td>Avtalsmallar</td>
				<td>
					<img src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/>
				</td>
			</tr>
			<tr>
				<td>Checklistor</td>
				<td>
					<img src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/>
				</td>
			</tr>

			<tr
				:key="optional.id"
				v-for="optional in optionals"
				:class="!addedOptionalsID.includes(optional.id) ? 'not-checked' : ''"
			>
				<td> {{ optional.name }} </td>
				<td>
					<img v-if="addedOptionalsID.includes(optional.id)" src="/wp-content/themes/custom-theme/dist/icons/green-checkmark.svg"/>
					<span v-else>Tillval</span>
				</td>
			</tr>
		</table>
		<div class="fp-order-info__price">
			<p class="fp-order-info__price-sek"> {{ thousandSeperator(totalPrice) }} kr/mån</p>
			<p class="fp-order-info__price-exclusive">Exklusive moms</p>
		</div>
	</section>
</div>
