<?php
/**
 * Popup (read more) for order flow.
 *
 * @package custom-theme
 */

?>

<div class="fp-popup-read-more">
	<section class="fp-popup-read-more__popup">
		<a
			href="#"
			class="fp-popup-read-more__close"
			@click="togglePopup(optional.id)"
		>
			Stäng fönster <img class="fp-popup-read-more__close-img" src="/wp-content/themes/custom-theme/dist/icons/close-icon.svg">
		</a>

		<h2 class="fp-popup-read-more--boldify fp-popup-read-more__title"> {{ optional.name }} </h2>
		<p class="fp-popup-read-more--no-margin-top"> {{ optional.description }} </p>

		<a
			:href="optional.link.url"
			class="fp-popup-read-more__popup-link"
		>
			Läs mer
		</a>
	</section>
</div>
