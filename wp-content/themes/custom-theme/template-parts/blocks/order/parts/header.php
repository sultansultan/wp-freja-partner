<?php
/**
 * Header for order flow.
 *
 * @package custom-theme
 */

?>

<section>
	<h1>Beställ paket</h1>
	<p><?php the_field( 'order_header_text' ); ?></p>
</section>
