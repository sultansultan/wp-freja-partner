<?php
/**
 * File for section 1 in order-flow.
 *
 * @package custom-theme
 */

?>
<p v-if="!loaded"> Laddar... </p>
<div class="fp-order-step-1" v-else>
	<?php require get_template_directory() . '/template-parts/blocks/order/parts/header.php'; ?>

	<h2 class="fp-bundle-form__heading">Välj paket</h2>

	<form class="fp-bundle-form" method="GET">
		<div :key="bundle.id" v-for="bundle in bundles">
			<input
				type="radio"
				:id="'bundle-' + bundle.id"
				name="bundle"
				:value="bundle.id"
				v-model="selectedBundleID"
			/>
			<label class="fp-bundle-form__label" :for="'bundle-' + bundle.id">
				<span class="fp-bundle-form__label-type">{{ bundle.name }}</span>
				<span class="fp-bundle-form__label-price">{{ thousandSeperator(bundle.price) }} kr/mån</span>
				<a
					class="fp-bundle-form__label-read"
					href="#"
					@click="togglePopup(bundle.id)"
				>
					Läs mer
				</a>

				<?php require get_template_directory() . '/template-parts/blocks/order/parts/popup-read-more-bundle.php'; ?>
			</label>
		</div>
	</form>

	<h2 class="fp-customer-form__heading">Dina uppgifter</h2>

	<div class="fp-order-step-1__error" v-if="errors.length">
		<b>Något gick fel, kolla över detta:</b>
		<ul>
			<li v-for="error in errors">{{ error }}</li>
		</ul>
	</div>

	<form class="fp-customer-form" method="POST">
		<div class="fp-customer-form__name">
			<input class="fp-customer-form__name__first" type="text" placeholder="Förnamn" v-model="customer.name"/>
			<input class="fp-customer-form__name__last" type="text" placeholder="Efternamn" v-model="customer.surname"/>
		</div>
		<input type="text" placeholder="Mejladress" v-model="customer.email"/>
		<input type="text" placeholder="Företag" v-model="customer.company"/>
		<input type="text" placeholder="Adress" v-model="customer.address"/>
		<div class="fp-customer-form__place">
			<input class="fp-customer-form__place__zip" type="text" placeholder="Postnummer" v-model="customer.zip"/>
			<input class="fp-customer-form__place__place" type="text" placeholder="Ort" v-model="customer.place"/>
		</div>
		<input type="text" placeholder="Organisationsnummer" v-model="customer.orgnr"/>
	</form>

	<?php require get_template_directory() . '/template-parts/blocks/order/parts/navigation.php'; ?>
</div>
