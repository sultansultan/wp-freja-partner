/**
 * Main js file for order flow.
 */

(function ($) {
	$(function () {

		let container = document.getElementById('fp-order');

		if(!container) {
			return;
		} else {
			const router = new VueRouter({
				routes: [{ path: "/:step" }],
			});
	
			new Vue({
				el: "#fp-order",
				router: router,
				created() {
					window.fetch('/wp-json/custom-theme/v1/bundles')
						.then(response => response.json())
						.then(data => {
							this.bundles = data
	
							this.loaded = true
						})
	
					window.fetch('/wp-json/custom-theme/v1/bundles/optionals')
						.then(response => response.json())
						.then(data => {
							this.optionals = data
						})
	
	
					this.setBundle()
				},
				data() {
					return {
						step: 1,
						errors: [],
						selectedBundleID: 1,
						steps: [
							{ step: 1, name: 'Välj paket' },
							{ step: 2, name: 'Tillval' },
							{ step: 3, name: 'Klart!' },
						],
						customer: {
							name: '',
							surname: '',
							email: '',
							company: '',
							address: '',
							zip: '',
							place: '',
							orgnr: '',
						},
						customerReady: false,
						bundles: [],
						optionals: [],
						addedOptionalsID: [],
						loaded: false,
					}
				},
				methods: {
					setBundle() {
						const url = new URL(window.location.href);
						this.selectedBundleID = url.searchParams.get('bundle');
					},
					togglePopup(bundleID) {
						let popup = document.querySelectorAll('.fp-popup-read-more');
	
						if(popup[bundleID].classList.contains('toggle')) {
							popup[bundleID].classList.remove('toggle');
						} else {
							popup[bundleID].classList.add('toggle');
						}
					},
					scrollToTop() {
						setTimeout(() => {
							window.scrollTo({ top: 100, left: 0, behavior: "smooth" });
						}, 300);
					},
					handleSectionChange(section) {
						const num = parseInt(section);
	
						if (num > 1) {
							this.go(num, true);
						} else if (!num || num < 2) {
							this.go(1);
						}
	
						this.scrollToTop();
					},
					go(num, force = false) {
						if (num <= this.step) {
							this.step = parseInt(num);
						}
						if (force) {
							this.step = parseInt(num);
						}
					},
					isActive(step) {
						return step === this.step;
					},
					goToNext(route) {
						this.validation();
	
						if(!this.errors.length) {
							this.customerReady = true;
	
							// Prevents going back in stepper when on last step.
							if(this.step !== this.steps.length) {
								this.$router.push(route);
							}
						}
					},
					validation() {
						this.errors = []
	
						let hasRequired
	
						for(let value in this.customer) {
							hasRequired = this.checkRequiredFields(value, this.customer[value]);
						}
	
						if(!this.validEmail(this.customer.email)) {
							this.errors.push('Fyll i en giltig mejladress.');
						}
	
						if(!this.validZip(this.customer.zip)) {
							this.errors.push('Fyll i ett giltig postnummer.');
						}
	
						if(!hasRequired) {
							this.errors.push('Fyll i alla obligatoriska fält.');
						}
					},
					checkRequiredFields(field, value) {
						return value
					},
					validEmail (email) {
						if(email.includes('@')) {
							let splitEmail = email.split('@');
	
							return splitEmail[0].length > 0 && splitEmail[1].length > 0;
						}
	
						return false;
					},
					validZip (zip) {
						return (zip.length === 5 && !isNaN(zip)) || /^\d{3} \d{2}$/.test(zip);
					},
					clearAddedOptionals () {
						Vue.set(this, 'addedOptionalsID', []);
					},
					sendOrder () {
						const data = {
							customer: this.customer,
							bundle: this.bundles[this.selectedBundleID],
							optionals: this.addedOptionals,
						}
	
						fetch('/wp-json/custom-theme/v1/bundles/order', {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json',
							},
							body: JSON.stringify(data),
						}).then((response) => {
							if (response) {
								console.log(response);
								this.goToNext('?step=' + this.steps.length);
							}
						});
					},
					arrayToString(list) {
						let array = [];
	
						// Get all text
						for(let i = 0; i < list.length; i++) {
							array.push(list[i].text);
						}
	
						let stringified = array.join(', ');
	
						return stringified;
					},
					thousandSeperator(number) {
						return number.toLocaleString('sv');
					}
				},
				computed: {
					nextStep() {
						return this.step + 1;
					},
					previousStep() {
						return this.step - 1;
					},
					selectedBundle () {
						return this.bundles[this.selectedBundleID];
					},
					totalPrice () {
						let addedExpenses = this.addedOptionals
							.map(optional => optional.price)
							.reduce((a, b) => a + b, 0)
	
						return this.selectedBundle.price + addedExpenses
					},
					addedOptionals () {
						return this.optionals
							.filter(optional => this.addedOptionalsID.includes(optional.id))
					}
				},
				watch: {
					"$route.params": function (newVal, oldVal) {
						this.handleSectionChange(this.$route.query.step);
					},
				}
			}).$route;
		}
	});
})(jQuery);