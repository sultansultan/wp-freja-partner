<?php
/**
 * Template for gutenberg block to show top banner.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-top-banner';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}

$custom_theme_queried_object          = get_queried_object();
$custom_theme_banner_background_img   = get_field( 'banner_background_img' ) ?: get_field( 'banner_background_img', $custom_theme_queried_object );// phpcs:ignore
$custom_theme_banner_background_title = get_field( 'banner_title' ) ?: get_field( 'banner_title', $custom_theme_queried_object );// phpcs:ignore
$custom_theme_banner_background_text  = get_field( 'banner_text' ) ?: get_field( 'banner_text', $custom_theme_queried_object );// phpcs:ignore
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>" style="background-image: url('<?php echo esc_attr( $custom_theme_banner_background_img ); ?>')">
	<div class="fp-top-banner__overlay" ></div>
	<h1 class="fp-top-banner__title text-white">
		<?php echo $custom_theme_banner_background_title ? esc_attr( $custom_theme_banner_background_title ) : ''; ?>
	</h1>
	<p class="fp-top-banner__text text-sm text-white">
		<?php echo $custom_theme_banner_background_text ? esc_attr( $custom_theme_banner_background_text ) : ''; ?>
	</p>
	<?php
	$custom_theme_button = get_field( 'button' );
	if ( $custom_theme_button ) :
		$custom_theme_button_target = $custom_theme_button['target'] ? $custom_theme_button['target'] : '_self';
		?>
		<a class="fp-top-banner__button fp-button fp-button--fixed-width fp-button--green" href="<?php echo esc_url( $custom_theme_button['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_button_target ); ?>">
			<?php echo esc_html( $custom_theme_button['title'] ); ?>
		</a>
	<?php endif; ?>
</div>
