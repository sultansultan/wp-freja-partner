<?php
/**
 * Template for gutenberg block hero.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-hero';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="main-wrapper">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
		<?php if ( get_field( 'videofile' ) ) : ?>
			<video class="<?php echo esc_attr( $custom_theme_class_name ); ?>__video" width="x" height="y" playsinline autoplay muted loop>
				<source src="<?php echo esc_attr( get_field( 'videofile' )['url'] ); //phpcs:ignore ?>" type="video/mp4">
			</video>
		<?php endif; ?>

		<?php
		if ( get_field( 'image' ) ) :
			$custom_theme_image = get_field( 'image' );
			?>
			<div class="fp-hero__img" style="background-image: url(<?php echo esc_url( $custom_theme_image['url'] ); ?>)"></div>
		<?php endif; ?>

		<section class="fp-hero__content">
			<h1 class="fp-hero__title">
				<?php the_field( 'title' ); ?>
			</h1>

			<?php
			$custom_theme_link = get_field( 'link' );

			if ( $custom_theme_link ) :
				$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
				?>
				<a class="fp-link fp-hero__link" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
					<span class="fp-link__text">
						<?php echo esc_html( $custom_theme_link['title'] ); ?>
					</span>

					<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
				</a>
			<?php endif; ?>
		</section>
	</div>
</div>
