<?php
/**
 * Template for gutenberg block for "Fråga juristen".
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-ask-law-mini';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div
	class="<?php echo esc_attr( $custom_theme_class_name ); ?>"
	style="background-image: url(<?php echo esc_url( get_field( 'ask_law_mini_background_image', 'options' ) ); ?>)"
>
	<h2 class="<?php echo esc_attr( $custom_theme_class_name ); ?>__title">
		<?php the_field( 'ask_law_mini_title', 'options' ); ?>
	</h2>

	<p class="<?php echo esc_attr( $custom_theme_class_name ); ?>__text">
		<?php the_field( 'ask_law_mini_text', 'options' ); ?>
	</p>

	<?php
	$custom_theme_link = get_field( 'ask_law_mini_link', 'options' );
	if ( $custom_theme_link ) :
		$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
		?>
		<a
			class="fp-button fp-button--fixed-width fp-button--light-grey"
			href="<?php echo esc_url( $custom_theme_link['url'] ); ?>"
			target="<?php echo esc_attr( $custom_theme_link_target ); ?>"
		>
			<img class="fp-ask-law__button-img" src="/frontend/src/icons/Chat-icon.svg"/>
			<?php echo esc_html( $custom_theme_link['title'] ); ?>
		</a>
	<?php endif; ?>
</div>
