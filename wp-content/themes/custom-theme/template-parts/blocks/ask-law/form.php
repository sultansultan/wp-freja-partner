<?php
/**
 * Template for gutenberg block for "Fråga juristen" form.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-ask-law-form';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="main-wrapper main-wrapper--off-white">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-duo fp-container">
		<section class="fp-duo__Left fp-ask-law-form__text">
			<h1> <?php the_field( 'title' ); ?> </h1>
			<p class="fp-ask-law-form__ingress"><?php the_field( 'ingress' ); ?></p>
			<p><?php the_field( 'text' ); ?></p>
		</section>
		<section class="fp-duo__right fp-ask-law-form__form">
			<?php the_field( 'contactform' ); ?>
		</section>
	</div>
</div>
