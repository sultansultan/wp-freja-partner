<?php
/**
 * Template for gutenberg block for "Fråga juristen".
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-ask-law';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-duo fp-duo--smaller-rows">
	<div class="fp-duo__left fp-ask-law__img" style="background-image: url('<?php echo esc_attr( get_field( 'image' ) ); ?>')">
	</div>
	<div class="fp-duo__right">
		<div class="fp-ask-law__text-container">
			<h2 class="fp-ask-law__title">
				<?php the_field( 'title' ); ?>
			</h2>

			<span class="fp-ask-law__text">
				<?php the_field( 'short_text' ); ?>
			</span>

			<?php
			$custom_theme_link = get_field( 'link' );

			if ( $custom_theme_link ) :
				$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
				?>
				<a class="fp-button fp-button--light" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
					<img class="fp-ask-law__button-img" src="/frontend/src/icons/Chat-icon.svg"/>

					<span>
						<?php echo esc_html( $custom_theme_link['title'] ); ?>
					</span>
				</a>
			<?php endif; ?>
		</div>
	</div>
</div>
