<?php
/**
 * Template for gutenberg block for text with cards.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-text-with-cards';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
	<div class="fp-text-with-cards__main">
		<?php $custom_theme_image = get_field( 'image' ); ?>
		<img class="fp-text-with-cards__image" src="<?php echo esc_url( $custom_theme_image['url'] ); ?>" alt="<?php echo esc_attr( $custom_theme_image['alt'] ); ?>" />

		<article class="fp-text-with-cards__content">
			<h2 class="fp-text-with-cards__content-title"> <?php the_field( 'title' ); ?> </h2>
			<p> <?php the_field( 'text' ); ?> </p>
			<?php
			if ( get_field( 'link' ) ) :
				$custom_theme_link        = get_field( 'link' );
				$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
				?>
				<a class="fp-link" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
					<span class="fp-link__text">
						<span class="fp-link__text">
							<?php echo esc_html( $custom_theme_link['title'] ); ?>
						</span>
					</span>

					<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
				</a>
			<?php endif; ?>
		</article>
	</div>

	<div class="fp-text-with-cards__cards">
		<div class="fp-text-with-cards__cards-background"></div>

		<div class="fp-text-with-cards__cards-container">
			<?php if ( have_rows( 'cards' ) ) : ?>
				<?php
				while ( have_rows( 'cards' ) ) :
					the_row();
					?>
					<article class="fp-text-with-cards__card">
						<h2 class="fp-text-with-cards__card-title"> <?php the_sub_field( 'title' ); ?> </h2>
						<p> <?php the_sub_field( 'text' ); ?> </p>

						<?php
							$custom_theme_card_link        = get_sub_field( 'link' );
							$custom_theme_card_link_target = $custom_theme_card_link['target'] ? $custom_theme_card_link['target'] : '_self';
						?>
						<a class="fp-button fp-button--grey fp-text-with-cards__card-button" href="<?php echo esc_url( $custom_theme_card_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_card_link_target ); ?>">
							<?php echo esc_html( $custom_theme_card_link['title'] ); ?>
						</a>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
