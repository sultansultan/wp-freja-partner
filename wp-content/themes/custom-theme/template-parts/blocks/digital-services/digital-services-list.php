<?php
/**
 * Template for gutenberg block to show digital services list.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-digital-services-list';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<?php
	$custom_theme_digital_services = get_field( 'digital_services_order', 'option' );
?>

<section class="<?php echo esc_attr( $custom_theme_class_name ); ?>--max-width">
	<?php if ( $custom_theme_digital_services ) : ?>
		<ul class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
			<h3>Alla digitala tjänster</h3>
			<?php
			foreach ( $custom_theme_digital_services as $custom_theme_digital_service ) :
				$custom_theme_digital_service_permalink = get_permalink( $custom_theme_digital_service->ID );
				$custom_theme_digital_service_title     = get_the_title( $custom_theme_digital_service->ID );
				?>
				<li class="fp-digital-services-list__item">
					<a class="fp-link" href="<?php echo esc_url( $custom_theme_digital_service_permalink ); ?>">
						<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
						<span class="fp-link__text text-sm text-darker-blue text-bold"><?php echo esc_attr( $custom_theme_digital_service_title ); ?></span>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</section>
