<?php
/**
 * Template for gutenberg block to show digital services.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-digital-services';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<?php
	$custom_theme_digital_services = get_field( 'digital_services_order', 'option' );
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>--max-width">
	<section class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
		<?php if ( $custom_theme_digital_services ) : ?>
			<?php
			foreach ( $custom_theme_digital_services as $custom_theme_digital_service ) :
				$custom_theme_digital_service_permalink = get_permalink( $custom_theme_digital_service->ID );
				$custom_theme_digital_service_title     = get_the_title( $custom_theme_digital_service->ID );
				$custom_theme_digital_service_excerpt   = get_the_excerpt( $custom_theme_digital_service->ID );
				$custom_theme_digital_service_action    = 'Fråga juristen' === $custom_theme_digital_service_title ? 'Ställ en fråga' : 'Läs mer';
				?>
				<article class="fp-digital-service">
					<a class="fp-digital-service--link" href="<?php echo esc_url( $custom_theme_digital_service_permalink ); ?>">
						<div class="fp-digital-service__content">
							<span>
								<h3 class="fp-digital-service__title text-darker-blue">
									<?php echo esc_attr( $custom_theme_digital_service_title ); ?>
								</h3>
								<p class="fp-digital-service__bread text-sm text-mirage">
									<?php echo esc_attr( $custom_theme_digital_service_excerpt ); ?>
								</p>
							</span>

							<p class="fp-digital-service__link text-sm text-darker-blue text-normal">
								<?php echo esc_attr( $custom_theme_digital_service_action ); ?>
								<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
							</p>
						</div>
					</a>
				</article>
			<?php endforeach; ?>
		<?php endif; ?>
	</section>
</div>

