<?php
/**
 * Template for gutenberg block hero.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-text-and-image';
if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}

// Side selector.
$custom_theme_side = 'left';

if ( get_field( 'left_right' ) ) {
	$custom_theme_side = 'right';
}
?>

<div class="<?php echo esc_attr( $custom_theme_class_name ); ?> fp-text-and-image__<?php echo esc_attr( $custom_theme_side ); ?> fp-duo">
	<div class="fp-duo__left">
		<div class="fp-text-and-image__img">
			<img src="<?php echo esc_attr( get_field( 'image' ) ); ?>" />
		</div>
	</div>
	<div class="fp-duo__right fp-duo__left__text fp-duo--text-center fp-duo--text-left">
		<div class="fp-duo__text-container">
			<h2 class="fp-text-and-image__title">
				<?php the_field( 'title' ); ?>
			</h2>

			<span class="fp-text-and-image__text">
				<?php the_field( 'textfield' ); ?>
			</span>

			<?php
			$custom_theme_link = get_field( 'link' );

			if ( $custom_theme_link ) :
				$custom_theme_link_target = $custom_theme_link['target'] ? $custom_theme_link['target'] : '_self';
				?>
				<a class="fp-link" href="<?php echo esc_url( $custom_theme_link['url'] ); ?>" target="<?php echo esc_attr( $custom_theme_link_target ); ?>">
					<span class="fp-link__text">
						<?php echo esc_html( $custom_theme_link['title'] ); ?>
					</span>

					<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
				</a>
			<?php endif; ?>
		</div>
	</div>
</div>
