<?php
/**
 * Template for gutenberg block for latest news.
 *
 * @package custom-theme
 */

$custom_theme_class_name = 'fp-latest-news';

if ( is_front_page() ) {
	$custom_theme_class_name .= ' fp-latest-news--front';
}

if ( ! empty( $block['className'] ) ) {
	$custom_theme_class_name .= ' ' . $block['className'];
}

if ( ! empty( $block['align'] ) ) {
	$custom_theme_class_name .= ' align' . $block['align'];
}
?>

<?php
$custom_theme_queried_object             = get_queried_object();
$custom_theme_latest_exclude_post_ids    = array();
$custom_theme_latest_ordered_posts       = get_field( 'latest_news_order', 'option' );
$custom_theme_latest_ordered_posts_count = 0;
$custom_theme_latest_posts               = array();


if ( 'post' === $custom_theme_queried_object->post_type ) {
	array_push( $custom_theme_latest_exclude_post_ids, $custom_theme_queried_object->ID );

	if ( $custom_theme_latest_ordered_posts ) {
		$custom_theme_latest_ordered_posts = array_filter(
			$custom_theme_latest_ordered_posts,
			function( $custom_theme_latest_ordered_post ) use ( $custom_theme_queried_object ) {// phpcs:ignore
				return $custom_theme_queried_object->ID !== $custom_theme_latest_ordered_post->ID;// phpcs:ignore
			}// phpcs:ignore
		);
	}
}

if ( $custom_theme_latest_ordered_posts ) {
	$custom_theme_latest_ordered_posts_count = count( $custom_theme_latest_ordered_posts );

	foreach ( $custom_theme_latest_ordered_posts as $custom_theme_latest_ordered_post ) {
		array_push( $custom_theme_latest_exclude_post_ids, $custom_theme_latest_ordered_post->ID );
	}
} else {
	$custom_theme_latest_ordered_posts = array();
}

if ( $custom_theme_latest_ordered_posts_count < 3 ) {
	$custom_theme_latest_posts = get_posts(
		array(
			'numberposts' => 3 - $custom_theme_latest_ordered_posts_count,
			'exclude'     => $custom_theme_latest_exclude_post_ids,
		)
	);
}

$custom_theme_latest_posts = array_merge( $custom_theme_latest_ordered_posts, $custom_theme_latest_posts );

?>

<div class="main-wrapper main-wrapper--off-white">
	<div class="<?php echo esc_attr( $custom_theme_class_name ); ?>">
		<section class="fp-articles fp-articles--mobile">
			<h2>Senaste nytt</h2>

			<div class="fp-latest-news__wrapper fp-latest-news__wrapper--desktop">
				<?php if ( $custom_theme_latest_posts ) : ?>
					<?php global $post;// phpcs:ignore ?>
					<?php
					foreach ( $custom_theme_latest_posts as $post ) : // phpcs:ignore
						setup_postdata( $post );
						get_template_part( 'template-parts/cards/content', $post->post_type );
						?>
					<?php endforeach; ?>

					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>

			<div class="fp-latest-news__wrapper fp-latest-news__wrapper--mobile">
				<?php if ( $custom_theme_latest_posts ) : ?>
					<?php global $post;// phpcs:ignore ?>
					<?php
					foreach ( $custom_theme_latest_posts as $post ) : // phpcs:ignore
						setup_postdata( $post );
						get_template_part( 'template-parts/cards/content', $post->post_type );
						?>
					<?php endforeach; ?>

					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</div>

			<a class="fp-link" href="<?php echo esc_url( get_home_url( null, '/nyheter' ) ); ?>">
					<span class="fp-link__text">
						Se alla nyheter
					</span>
				<img class="fp-link__arrow" src="/frontend/src/icons/Arrow-icon.svg"/>
			</a>
		</section>
	</div>
</div>
