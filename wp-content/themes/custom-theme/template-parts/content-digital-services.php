<?php
/**
 * Template part for displaying the single digital service
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package custom-theme
 */

?>

<?php
	$custom_theme_article_category       = get_the_category();
	$custom_theme_queried_object         = get_queried_object();
	$custom_theme_top_banner_curved_img  = get_field( 'top_banner_curved_img', $custom_theme_queried_object );
	$custom_theme_top_banner_curved_text = get_field( 'top_banner_curved_text', $custom_theme_queried_object );
	$custom_theme_iframe                 = get_field( 'post_video', $custom_theme_queried_object );
?>

<div class="fp-single-digital-service--max-width">
	<article class="fp-single-digital-service" id="article-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( $custom_theme_top_banner_curved_img ) : ?>
			<div class="fp-single-digital-service__img" style="background-image: url('<?php echo esc_attr( $custom_theme_top_banner_curved_img ); ?>')">
				<?php if ( $custom_theme_top_banner_curved_text ) : ?>
					<h1 class="text-white"> <?php echo esc_attr( $custom_theme_top_banner_curved_text ); ?> </h1>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<div class="fp-single-digital-service-content">
			<div class="fp-single-digital-service-content__left">
				<?php the_content(); ?>
			</div>
			<div class="fp-single-digital-service-content__right">
				<?php if ( get_field( 'post_video', $custom_theme_queried_object ) ) : ?>
					<div class="fp-single-digital-service-content__video">
						<?php require get_template_directory() . '/template-parts/components/video.php'; ?>
					</div>
				<?php endif; ?>

				<?php
				custom_theme_get_custom_block(
					'template-parts/blocks/digital-cta/digital-cta'
				);
				?>
			</div>
		</div>
	</article><!-- #post-<?php the_ID(); ?> -->
</div>

<?php
	custom_theme_get_custom_block(
		'template-parts/blocks/digital-services/digital-services-list'
	);

	custom_theme_get_custom_block(
		'template-parts/blocks/packages/packages'
	);
	?>
