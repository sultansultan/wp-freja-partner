<?php
/**
 * SMTP settings for wp without plugins
 *
 * @package custom-theme
 */

if ( defined( 'SMTP_SETTINGS' ) && SMTP_SETTINGS ) {
	add_action( 'phpmailer_init', 'custom_theme_send_smtp_email' );

	/**
	 * Add smtp settings to WordPress through wp-config
	 * Define following:
	 * define( 'SMTP_SETTINGS', true) Enabled smtp settings
	 * define( 'SMTP_USER', 'user@example.com' )  Username to use for SMTP authentication
	 * define( 'SMTP_PASS', 'smtp password' ) Password to use for SMTP authentication
	 * define( 'SMTP_HOST', 'smtp.example.com' ) The hostname of the mail server
	 * define( 'SMTP_FROM', 'website@example.com' ) SMTP From email address
	 * define( 'SMTP_NAME', 'e.g Website Name' ) SMTP From name
	 * define( 'SMTP_PORT', '25' ) SMTP port number - likely to be 25, 465 or 587
	 * define( 'SMTP_SECURE','tls' ) Encryption system to use - ssl or tls
	 * define( 'SMTP_AUTH', true ) Use SMTP authentication (true|false)
	 *
	 * @param type $phpmailer Mailer object.
	 */
	function custom_theme_send_smtp_email( $phpmailer ) {
		$phpmailer->isSMTP();
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->Host = SMTP_HOST;
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->SMTPAuth = SMTP_AUTH === 'true' || SMTP_AUTH === true;
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->Port = (int) SMTP_PORT;
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->Username = SMTP_USER;
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->Password = SMTP_PASS;
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->SMTPSecure = SMTP_SECURE;
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->From = SMTP_FROM;
		// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
		$phpmailer->FromName = SMTP_NAME;
	}
}
