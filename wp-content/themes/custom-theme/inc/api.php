<?php
/**
 * File for everything API related.
 *
 * @package custom-theme
 */

/**
 * Register API endpoints.
 */
function custom_theme_register_route() {
	register_rest_route(
		'custom-theme/v1',
		'/bundles',
		array(
			'methods'             => 'GET',
			'callback'            => 'custom_theme_get_bundles',
			'permission_callback' => '__return_true',
		)
	);

	register_rest_route(
		'custom-theme/v1',
		'/bundles/optionals',
		array(
			'methods'             => 'GET',
			'callback'            => 'custom_theme_get_optionals',
			'permission_callback' => '__return_true',
		)
	);

	register_rest_route(
		'custom-theme/v1',
		'/bundles/order',
		array(
			'methods'             => 'POST',
			'callback'            => 'custom_theme_api_send_order',
			'permission_callback' => '__return_true',
		)
	);
}
add_action( 'rest_api_init', 'custom_theme_register_route' );

/**
 * Fetch bundle data from options.
 */
function custom_theme_get_bundles() {
	if ( have_rows( 'bundles', 'options' ) ) {
		$result = array();

		while ( have_rows( 'bundles', 'options' ) ) {
			the_row();

			$result[ get_sub_field( 'id' ) ] = array(
				'name'        => get_sub_field( 'name' ),
				'price'       => intval( get_sub_field( 'price' ) ),
				'hours'       => intval( get_sub_field( 'hours' ) ),
				'credits'     => intval( get_sub_field( 'credits' ) ),
				'id'          => get_sub_field( 'id' ),
				'description' => get_sub_field( 'description' ),
				'small_link'  => get_sub_field( 'small_link' ),
				'whats_init'  => get_sub_field( 'whats_init' ),
			);
		}

		return $result;
	}
}

/**
 * Fetch optionals data from options.
 */
function custom_theme_get_optionals() {
	if ( have_rows( 'optionals', 'options' ) ) {
		$result = array();

		while ( have_rows( 'optionals', 'options' ) ) {
			the_row();

			$result[ get_sub_field( 'id' ) ] = array(
				'id'          => get_sub_field( 'id' ),
				'name'        => get_sub_field( 'name' ),
				'price'       => intval( get_sub_field( 'price' ) ),
				'description' => get_sub_field( 'description' ),
				'link'        => get_sub_field( 'link' ),
			);
		}

		return $result;
	}
}

/**
 * Mail.
 */
function custom_theme_api_send_order() {
	$data = json_decode( file_get_contents( 'php://input' ), true );

	$email = WP_Mail::init()
		->to( get_field( 'order_email', 'options' ) )
		->from( get_field( 'order_email_from', 'options' ) )
		->subject( 'Ny order | ' . $data['customer']['company'] )
		->template(
			get_template_directory() . '/mail/order.php',
			array(
				'data' => $data,
			)
		)
		->send();

	$confirmation = WP_Mail::init()
		->to( $data['customer']['email'] )
		->from( get_field( 'order_email_from', 'options' ) )
		->subject( 'Bekräftelse | Freja Partner' )
		->template(
			get_template_directory() . '/mail/confirmation.php',
			array(
				'data' => $data,
			)
		)
		->send();

	return $data;
}
