<?php
/**
 * Register custom gutenberg blocks based on ACF.
 *
 * @package custom-theme
 */

acf_register_block_type(
	array(
		'name'            => 'hero',
		'title'           => __( 'Hero', 'custom-theme' ),
		'description'     => __( 'Nice hero', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/hero/hero.php',
		'icon'            => 'menu',
		'keywords'        => array( 'hero' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'text-and-image',
		'title'           => __( 'Text och bild', 'custom-theme' ),
		'description'     => __( 'Block för text och bild', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/text-and-image/text-and-image.php',
		'icon'            => 'align-pull-left',
		'keywords'        => array( 'text-and-image' ),
	)
);


acf_register_block_type(
	array(
		'name'            => 'we-can',
		'title'           => __( 'Vi har/Vi kan', 'custom-theme' ),
		'description'     => __( 'Vi har/Vi kan', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/we-can/we-can.php',
		'icon'            => 'menu',
		'keywords'        => array( 'we-can' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'video-and-text',
		'title'           => __( 'Video och Text', 'custom-theme' ),
		'description'     => __( 'Block för text med tillhörande video.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/video-and-text/video-and-text.php',
		'icon'            => 'format-video',
		'keywords'        => array( 'video-and-text' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'ask-law',
		'title'           => __( 'Fråga juristen', 'custom-theme' ),
		'description'     => __( 'Block för "Fråga juristen"', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/ask-law/ask-law.php',
		'icon'            => 'format-status',
		'keywords'        => array( 'ask-law' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'digital-legal',
		'title'           => __( 'Digitala juristtjänster', 'custom-theme' ),
		'description'     => __( 'Block för "Digitala juristtjänster"', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/digital-legal/digital-legal.php',
		'icon'            => 'menu',
		'keywords'        => array( 'digital-legal' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'quotes',
		'title'           => __( 'Citat', 'custom-theme' ),
		'description'     => __( 'Block för att visa citat.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/quotes/quotes.php',
		'icon'            => 'images-alt',
		'keywords'        => array( 'quotes' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'latest-news',
		'title'           => __( 'Senaste nytt', 'custom-theme' ),
		'description'     => __( 'Block för att visa de senaste nyheterna.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/latest-news/latest-news.php',
		'icon'            => 'admin-site-alt',
		'keywords'        => array( 'quotes' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'packages',
		'title'           => __( 'Våra paketlösningar', 'custom-theme' ),
		'description'     => __( 'Block för att visa paketlösningar.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/packages/packages.php',
		'icon'            => 'products',
		'keywords'        => array( 'quotes' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'top-banner',
		'title'           => __( 'Topp baner', 'custom-theme' ),
		'description'     => __( 'Block för att visa topp baner.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/banner/top-banner.php',
		'icon'            => 'align-full-width',
		'keywords'        => array( 'top-banner' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'digital-services',
		'title'           => __( 'Digitala tjänster', 'custom-theme' ),
		'description'     => __( 'Block för att visa digitala tjänster.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/digital-services/digital-services.php',
		'icon'            => 'screenoptions',
		'keywords'        => array( 'digital-services' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'commercial-law',
		'title'           => __( 'Affärsjuridik', 'custom-theme' ),
		'description'     => __( 'Block för att visa Affärsjuridik.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/commercial-law/commercial-law.php',
		'icon'            => 'screenoptions',
		'keywords'        => array( 'commercial-law' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'commercial-law-listing',
		'title'           => __( 'Affärsjuridik Taxonomy Lista', 'custom-theme' ),
		'description'     => __( 'Block för att lista taxonomy för affärsjuridik.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/commercial-law/commercial-law-listing.php',
		'icon'            => 'screenoptions',
		'keywords'        => array( 'commercial-law-listing' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'digital-services-list',
		'title'           => __( 'Lista över digitala tjänster', 'custom-theme' ),
		'description'     => __( 'Block för att visa Lista över digitala tjänster.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/digital-services/digital-services-list.php',
		'icon'            => 'screenoptions',
		'keywords'        => array( 'digital-services-list' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'ask-law-form',
		'title'           => __( 'Kontaktformulär, Fråga juristen', 'custom-theme' ),
		'description'     => __( 'Block för att visa formulär för att kontakta juristen.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/ask-law/form.php',
		'icon'            => 'format-chat',
		'keywords'        => array( 'ask-law-form' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'contact-info',
		'title'           => __( 'Kontakt', 'custom-theme' ),
		'description'     => __( 'Block för att visa kontaktinformation.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/contact/contact-info.php',
		'icon'            => 'email',
		'keywords'        => array( 'contact-info' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'contact-form',
		'title'           => __( 'Kontaktformulär', 'custom-theme' ),
		'description'     => __( 'Block för kontaktformulär.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/contact/contact-form.php',
		'icon'            => 'email',
		'keywords'        => array( 'contact-form' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'coworkers',
		'title'           => __( 'Våra medarbetare', 'custom-theme' ),
		'description'     => __( 'Block för att visa våra medarbetare', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/contact/coworkers.php',
		'icon'            => 'admin-users',
		'keywords'        => array( 'coworkers' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'buttons',
		'title'           => __( 'Knappblock', 'custom-theme' ),
		'description'     => __( 'Block för att visa knappar', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/buttons/buttons.php',
		'icon'            => 'external',
		'keywords'        => array( 'buttons' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'packages-and-prices',
		'title'           => __( 'Paket & Priser', 'custom-theme' ),
		'description'     => __( 'Block för att visa paket & priser.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/packages/packages-and-prices.php',
		'icon'            => 'archive',
		'keywords'        => array( 'package-and-prices' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'order',
		'title'           => __( 'Beställ', 'custom-theme' ),
		'description'     => __( 'En komponent som tar användaren genom köp.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/order/order.php',
		'icon'            => 'hammer',
		'keywords'        => array( 'order' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'text-with-cards',
		'title'           => __( 'Text med kort', 'custom-theme' ),
		'description'     => __( 'Block som har text, bild och kort.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/text-with-cards/text-with-cards.php',
		'icon'            => 'screenoptions',
		'keywords'        => array( 'text-with-cards' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'showcase',
		'title'           => __( 'Showcaseblock', 'custom-theme' ),
		'description'     => __( 'Block som visar bild omgiven med text.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/showcase/showcase.php',
		'icon'            => 'cover-image',
		'keywords'        => array( 'showcase' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'collab',
		'title'           => __( 'Samarbetspartners', 'custom-theme' ),
		'description'     => __( 'Block som visar samarbetspartners.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/collab/collab.php',
		'icon'            => 'businesswoman',
		'keywords'        => array( 'collab' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'text-content',
		'title'           => __( 'text-block', 'custom-theme' ),
		'description'     => __( 'Block som visar text.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/text-content/text-content.php',
		'icon'            => 'businesswoman',
		'keywords'        => array( 'text-content' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'text-beside-cards',
		'title'           => __( 'Text bredvid kort', 'custom-theme' ),
		'description'     => __( 'Block som visar text bredvid max. 3 kort.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/text-beside-cards/text-beside-cards.php',
		'icon'            => 'align-pull-right',
		'keywords'        => array( 'text-beside-cards' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'text-with-table',
		'title'           => __( 'Text med tabell', 'custom-theme' ),
		'description'     => __( 'Block som visar text med tabell mitt i.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/text-with-table/text-with-table.php',
		'icon'            => 'media-text',
		'keywords'        => array( 'text-with-table' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'legalroom-content',
		'title'           => __( 'Legalroom innehåll (Text + Bild)', 'custom-theme' ),
		'description'     => __( 'Block som visar bild bredvid text.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/legalroom-content/legalroom-content.php',
		'icon'            => 'media-text',
		'keywords'        => array( 'legalroom-content' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'digital-cta',
		'title'           => __( 'Digital CTA', 'custom-theme' ),
		'description'     => __( 'CTA till digitala tjänster-singelsidor.', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/digital-cta/digital-cta.php',
		'icon'            => 'admin-post',
		'keywords'        => array( 'digital-cta' ),
	)
);

acf_register_block_type(
	array(
		'name'            => 'just-text',
		'title'           => __( 'Anpassat textblock (för Freja Partner)', 'custom-theme' ),
		'description'     => __( 'Anpassat textblock (för Freja Partner).', 'custom-theme' ),
		'category'        => 'freja-layout',
		'render_template' => get_template_directory() . '/template-parts/blocks/just-text/just-text.php',
		'icon'            => 'media-text',
		'keywords'        => array( 'just-text' ),
	)
);
