<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package custom-theme
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function custom_theme_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'custom_theme_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function custom_theme_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'custom_theme_pingback_header' );

/**
 * Icon generator used with symbols file in dist.
 *
 * @param string  $custom_theme_icon icon name.
 * @param boolean $custom_theme_echo use echo or return.
 */
function custom_theme_icon( $custom_theme_icon = '', $custom_theme_echo = true ) {
	$icon_markup  = sprintf( '<svg class="icon %s">', $custom_theme_icon );
	$icon_markup .= sprintf( '<use xlink:href="./frontend/dist/icons/svg-symbols.svg#%s"></use>', $custom_theme_icon );
	$icon_markup .= '</svg>';
	if ( $custom_theme_echo ) {
		echo $icon_markup; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	} else {
		return $icon_markup;
	}
}

/**
 * Custom_theme builder
 *
 * @param int $custom_theme_post id passed through hook.
 */
function custom_theme_builder( $custom_theme_post = null ) {
	if ( have_rows( 'layout' ) ) {
		while ( have_rows( 'layout' ) ) {
			the_row();
				get_template_part( 'template-parts/layouts/content', get_row_layout() );
		}
	}
}

/**
 * Admin margin - compensating for admin bar
 *
 * Adding as class name which adds margin top
 */
function custom_theme_logged_in_margin() {
	if ( is_user_logged_in() ) {
		echo esc_attr( ' logged-in-margin' );
	}
}

/**
 * Get youtube ID
 *
 * @param string $url a normal youtube url.
 */
function custom_theme_get_yt_id( $url ) {
	$obj = wp_parse_url( $url );
	return preg_replace( '/v=/', '', substr( $obj['path'], 7, 15 ) );
}
