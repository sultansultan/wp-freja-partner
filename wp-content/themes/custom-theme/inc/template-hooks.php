<?php
/**
 * Template hooks included in template files
 *
 * @package custom-theme
 */

/**
 * Custom theme hook name
 */
add_action( 'custom_theme_hook_name', 'custom_theme_builder', 1, 10 );
