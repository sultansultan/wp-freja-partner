<?php
/**
 * Import custom post types.
 *
 * @package custom-theme.
 */

/**
 * Import post type digital services.
 */
require get_template_directory() . '/inc/post-types/digital-services.php';

/**
 * Import post type commercial law.
 */
require get_template_directory() . '/inc/post-types/commercial-law.php';

/**
 * Import post type coworkers.
 */
require get_template_directory() . '/inc/post-types/coworkers.php';

/**
 * Import post type clientcases.
 */
require get_template_directory() . '/inc/post-types/clientcases.php';

