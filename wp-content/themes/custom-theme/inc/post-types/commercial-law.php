<?php
/**
 * Register a custom post type called commercial law
 *
 * @package custom-theme.
 */

/**
 * Register buildings post type
 */
function custom_theme_commercial_law_init() {
	$labels = array(
		'name'               => _x( 'Affärsjuridik', 'Post type general name', 'custom-theme' ),
		'singular_name'      => _x( 'Affärsjuridik', 'Post type singular name', 'custom-theme' ),
		'menu_name'          => _x( 'Affärsjuridik', 'Admin Menu text', 'custom-theme' ),
		'name_admin_bar'     => _x( 'Affärsjuridik', 'Add New on Toolbar', 'custom-theme' ),
		'add_new'            => __( 'Add New', 'custom-theme' ),
		'add_new_item'       => __( 'Add New ', 'custom-theme' ),
		'new_item'           => __( 'New', 'custom-theme' ),
		'edit_item'          => __( 'Edit', 'custom-theme' ),
		'view_item'          => __( 'View', 'custom-theme' ),
		'all_items'          => __( 'All', 'custom-theme' ),
		'search_items'       => __( 'Search', 'custom-theme' ),
		'parent_item_colon'  => __( 'Parent:', 'custom-theme' ),
		'not_found'          => __( 'No item found.', 'custom-theme' ),
		'not_found_in_trash' => __( 'No item found in Trash.', 'custom-theme' ),
	);

	$args = array(
		'labels'       => $labels,
		'public'       => true,
		'show_ui'      => true,
		'show_in_menu' => true,
		'show_in_rest' => true,
		'has_archive'  => false,
		'hierarchical' => false,
		'supports'     => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'   => array( 'field' ),
		'rewrite'      => array(
			'slug'       => 'affarsjuridik/%field%',
			'with_front' => false,
		),
		'menu_icon'    => 'dashicons-screenoptions',
	);

	register_post_type( 'commercial-law', $args );

	flush_rewrite_rules();
}

/**
 * Register commercial law taxonomies
 */
function custom_theme_taxonomies() {
	$labels = array(
		'name'              => _x( 'Område', 'taxonomy general name', 'custom-theme' ),
		'singular_name'     => _x( 'Område', 'taxonomy singular name', 'custom-theme' ),
		'search_items'      => __( 'Search', 'custom-theme' ),
		'all_items'         => __( 'All', 'custom-theme' ),
		'parent_item'       => __( 'Parent', 'custom-theme' ),
		'parent_item_colon' => __( 'Parent:', 'custom-theme' ),
		'edit_item'         => __( 'Edit', 'custom-theme' ),
		'update_item'       => __( 'Update', 'custom-theme' ),
		'add_new_item'      => __( 'Add New', 'custom-theme' ),
		'new_item_name'     => __( 'New Name', 'custom-theme' ),
		'menu_name'         => __( 'Område', 'custom-theme' ),
	);

	register_taxonomy(
		'field',
		array( 'commercial-law' ),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => array(
				'slug'       => 'affarsjuridik',
				'with_front' => false,
			),
		)
	);
}
add_action( 'init', 'custom_theme_taxonomies', 0 );

add_action( 'init', 'custom_theme_commercial_law_init' );

/**
 * Change permalinks.
 *
 * @param string $post_link Post link.
 * @param array  $post Array with post assets.
 */
function custom_theme_change_permalinks( $post_link, $post ) {
	if ( is_object( $post ) && 'commercial-law' === $post->post_type ) {
		$terms = wp_get_object_terms( $post->ID, 'field' );
		if ( $terms ) {
			return str_replace( '%field%', $terms[0]->slug, $post_link );
		}
	}
	return $post_link;
}
add_filter( 'post_type_link', 'custom_theme_change_permalinks', 1, 2 );
