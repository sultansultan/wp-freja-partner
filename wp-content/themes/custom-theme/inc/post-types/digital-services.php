<?php
/**
 * Register apartment post type.
 *
 * @package custom-theme
 */

/**
 * Register apartment post type.
 */
function custom_theme_digital_services_post_type() {

	$custom_theme_post_args = array(
		'labels'       => array(
			'name'               => _x( 'Digitala tjänster', 'Post type general name', 'custom-theme' ),
			'singular_name'      => _x( 'Digital tjänst', 'Post type singular name', 'custom-theme' ),
			'menu_name'          => _x( 'Digitala tjänster', 'Admin Menu text', 'custom-theme' ),
			'name_admin_bar'     => _x( 'Digitala tjänster', 'Add New on Toolbar', 'custom-theme' ),
			'add_new'            => __( 'Add New', 'custom-theme' ),
			'add_new_item'       => __( 'Add New', 'custom-theme' ),
			'new_item'           => __( 'New', 'custom-theme' ),
			'edit_item'          => __( 'Edit', 'custom-theme' ),
			'view_item'          => __( 'View', 'custom-theme' ),
			'all_items'          => __( 'All', 'custom-theme' ),
			'search_items'       => __( 'Search', 'custom-theme' ),
			'parent_item_colon'  => __( 'Parent:', 'custom-theme' ),
			'not_found'          => __( 'No item found.', 'custom-theme' ),
			'not_found_in_trash' => __( 'No item found in Trash.', 'custom-theme' ),
		),
		'public'       => true,
		'has_archive'  => false,
		'hierarchical' => false,
		'supports'     => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'rewrite'      => array( 'slug' => 'digitala-tjänster' ),
		'menu_icon'    => 'dashicons-screenoptions',
		'show_in_rest' => true,

	);
	register_post_type( 'digital-services', $custom_theme_post_args );
}
add_action( 'init', 'custom_theme_digital_services_post_type' );
