<?php
/**
 * Register client cases post type.
 *
 * @package custom-theme.
 */

/**
 * Register client cases post type
 */
function custom_theme_clientcases_init() {
	$labels = array(
		'name'               => _x( 'Våra Kundcase', 'Post type general name', 'custom-theme' ),
		'singular_name'      => _x( 'Kundcase', 'Post type singular name', 'custom-theme' ),
		'menu_name'          => _x( 'Våra kundcase', 'Admin Menu text', 'custom-theme' ),
		'name_admin_bar'     => _x( 'Våra kundcase', 'Add New on Toolbar', 'custom-theme' ),
		'add_new'            => __( 'Add New', 'custom-theme' ),
		'add_new_item'       => __( 'Add New ', 'custom-theme' ),
		'new_item'           => __( 'New', 'custom-theme' ),
		'edit_item'          => __( 'Edit', 'custom-theme' ),
		'view_item'          => __( 'View', 'custom-theme' ),
		'all_items'          => __( 'All', 'custom-theme' ),
		'search_items'       => __( 'Search', 'custom-theme' ),
		'parent_item_colon'  => __( 'Parent:', 'custom-theme' ),
		'not_found'          => __( 'No item found.', 'custom-theme' ),
		'not_found_in_trash' => __( 'No item found in Trash.', 'custom-theme' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_rest'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'kundcase' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail' ),
		'menu_icon'          => 'dashicons-admin-users',
	);

	register_post_type( 'clientcases', $args );
}

add_action( 'init', 'custom_theme_clientcases_init' );
