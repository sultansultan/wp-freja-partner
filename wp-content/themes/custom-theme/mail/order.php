<?php
/**
 * Mail template for order.
 *
 * @package custom-theme
 */

?>

<p> <b><?php echo esc_html( $data['customer']['company'] ); ?></b> har precis lagt en beställning! </p>
<hr/>
<h3>Beställning</h3>
<p><b>Paket: </b> <?php echo esc_html( $data['bundle']['name'] ); ?></p>
<p><b>Tillval: </b></p>
<?php foreach ( $data['optionals'] as $custom_theme_optional ) { ?>
	<p> <?php echo esc_html( $custom_theme_optional['name'] ); ?> </p>
<?php } ?>

<h3>Kontaktuppgifter</h3>
<p><?php echo esc_html( $data['customer']['company'] ); ?> <br/>
<?php echo esc_html( $data['customer']['orgnr'] ); ?> <br/>
<?php echo esc_html( $data['customer']['address'] ); ?> <br/>
<?php echo esc_html( $data['customer']['zip'] ) . ' ' . esc_html( $data['customer']['place'] ); ?></p>

<p>
	<?php echo esc_html( $data['customer']['name'] ) . ' ' . esc_html( $data['customer']['surname'] ); ?> <br/>
	<?php echo esc_html( $data['customer']['email'] ); ?>
</p>
