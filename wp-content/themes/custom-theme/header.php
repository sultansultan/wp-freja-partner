<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package custom-theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5PSXMX9');</script>
	<!-- End Google Tag Manager -->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5PSXMX9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="page" class="site">
	<header id="masthead" class="site-header">
		<div id="fp-header" class="fp-header">
			<div class="fp-header--max-width">
				<a class="fp-header__branding" href="/">
					<img class="fp-header__branding-img" src="/wp-content/themes/custom-theme/dist/icons/Freja-Partner-logo-black.svg">
				</a>

				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<svg class="menu-toggle__open" xmlns="http://www.w3.org/2000/svg" width="25" height="18" viewBox="0 0 25 18">
						<line id="Line_5" data-name="Line 5" x2="20" transform="translate(4 17)" fill="none" stroke="#0f2d3f" stroke-linecap="round" stroke-width="2"/>
						<line id="Line_6" data-name="Line 6" x2="13" transform="translate(11 9)" fill="none" stroke="#0f2d3f" stroke-linecap="round" stroke-width="2"/>
						<line id="Line_7" data-name="Line 7" x2="23" transform="translate(1 1)" fill="none" stroke="#0f2d3f" stroke-linecap="round" stroke-width="2"/>
					</svg>

					<svg class="menu-toggle__close" xmlns="http://www.w3.org/2000/svg" width="19.828" height="19.828" viewBox="0 0 19.828 19.828">
						<line id="Line_91" data-name="Line 91" x2="17" y2="16.5" transform="translate(1.414 1.664)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"/>
						<line id="Line_92" data-name="Line 92" x2="17" y2="16.5" transform="translate(18.164 1.414) rotate(90)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"/>
					</svg>
				</button>

				<nav id="site-navigation" class="fp-header__main-navigation">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
							'link_before'    => '<img class="back-arrow" src="/wp-content/themes/custom-theme/dist/icons/Arrow-icon.svg">',
							'link_after'     => '<img class="to-arrow" src="/wp-content/themes/custom-theme/dist/icons/Arrow-icon.svg">',
						)
					);
					?>
				</nav>

				<?php
				$custom_theme_header_settings = get_field( 'header', 'options' );

				if ( $custom_theme_header_settings ) :
					?>
					<a class="fp-header__ask" href="<?php echo esc_url( $custom_theme_header_settings['ask_law_link']['url'] ); ?>">
						<img class="fp-header__ask-img" src="/wp-content/themes/custom-theme/dist/icons/Chat-icon.svg">
						<span class="fp-header__ask-grouping">
							Fråga Juristen
							<span class="fp-header__ask-toggle-text">Kostnadsfri rådgivning</span>
						</span>
					</a>

					<a class="fp-header__login" href="<?php echo esc_url( $custom_theme_header_settings['login_link']['url'] ); ?>">
						Logga in
						<span class="fp-header__login-icon">
							<img class="fp-header__login-icon-img" src="/wp-content/themes/custom-theme/dist/icons/Key-icon.svg"/>
						</span>
					</a>
				<?php endif; ?>

				<div class="fp-header__socials">
					<?php
					$custom_theme_footer_header_settings = get_field( 'header_footer', 'options' );

					if ( $custom_theme_footer_header_settings ) :
						?>
						<span>
							<?php echo esc_html( $custom_theme_footer_header_settings['copyright'] ); ?>

							<a class="fp-header__socials-link" href="<?php echo esc_url( $custom_theme_footer_header_settings['integrity_cookies']['url'] ); ?>">
								Integritet & kakor.
							</a>
						</span>
						<span class="fp-header__socials-grouping">
							<?php if ( have_rows( 'header_footer', 'options' ) ) : ?>
								<?php
								while ( have_rows( 'header_footer', 'options' ) ) :
									the_row();
									?>
									<?php if ( have_rows( 'social_media_links' ) ) : ?>
										<?php
										while ( have_rows( 'social_media_links' ) ) :
											the_row();
											$custom_theme_socials_link        = get_sub_field( 'link' );
											$custom_theme_socials_link_target = $custom_theme_socials_link['target'] ? $custom_theme_socials_link['target'] : '_self'; // phpcs:ignore
											?>
											<a
												class="fp-header__socials-link"
												target="<?php echo esc_attr( $custom_theme_socials_link_target ); ?>"
												href="<?php echo esc_url( $custom_theme_socials_link['url'] ); ?>"
											>
													<?php echo esc_html( $custom_theme_socials_link['title'] ); ?>
											</a>
										<?php endwhile; ?>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						</span>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</header>

	<div id="content" class="site-content">
