<?php
/**
 * The template for displaying category pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package custom-theme
 */

get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main site-main--no-padding site-main--background-color">
		<header class="fp-articles-header">
			<?php
				custom_theme_get_custom_block(
					'template-parts/blocks/banner/top-banner'
				);
				?>
			<?php
				custom_theme_get_custom_template(
					'template-parts/categories-nav',
					get_queried_object()->slug
				);
				?>
		</header>

		<section class="fp-articles">

			<?php if ( have_posts() ) : ?>

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
					* Include the Post-Type-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Type name) and that will be used instead.
					*/
					get_template_part( 'template-parts/cards/content', get_post_type() );
					?>

				<?php endwhile;// phpcs:ignore ?>

				<?php
				// phpcs:ignore
				the_posts_pagination(
					array(
							'screen_reader_text' => __( ' ' ),// phpcs:ignore
							'prev_text'          => __( '<' ),// phpcs:ignore
							'next_text'          => __( '>' ),// phpcs:ignore
					)
				);
				?>
				<?php
			else :
				get_template_part( 'template-parts/content', 'none' );
				?>

			<?php endif; ?>

		</section>

	</main><!-- #main -->
</div><!-- #primary -->
</div>
<?php
get_footer();
