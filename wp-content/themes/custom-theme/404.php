<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package custom-theme
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main">
			<section class="error-404 not-found fp-not-found">
				<header class="fp-not-found__header">
					<h1 class="fp-not-found__title">
						<?php the_field( '404_title', 'options' ); ?>
					</h1>
				</header>

				<?php if ( get_field( '404_text', 'options' ) ) : ?>
					<div class="fp-not-found__content">
						<?php the_field( '404_text', 'options' ); ?>
					</div>
				<?php endif; ?>
			</section>

			<?php
			custom_theme_get_custom_block(
				'template-parts/blocks/we-can/we-can'
			);
			?>
		</main>
	</div>

<?php
get_footer();
