<?php
/**
 * The template for displaying taxonomy field
 *
 * @package custom-theme
 */

$custom_theme_queried_object                      = get_queried_object();
$custom_theme_commercial_law_field_featured_image = get_field( 'commercial_law_featured_image', $custom_theme_queried_object );
$custom_theme_commercial_law_field_name           = $custom_theme_queried_object->name;
$custom_theme_commercial_law_field_description    = $custom_theme_queried_object->description;
$custom_theme_args                                = array(
	'post_type'      => 'commercial-law',
	'posts_per_page' => -1,
	'tax_query' => array( // phpcs:ignore
		array(
			'taxonomy' => 'field',
			'field'    => 'slug',
			'terms'    => $custom_theme_queried_object->slug,
		),
	),
);

$custom_theme_commercial_law_field_posts = get_posts( $custom_theme_args );
get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main site-main--no-padding site-main--background-color">
		<?php
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<div class="breadcrumbs breadcrumbs--grey">', '</div>' );
		}
		?>

		<article class="fp-single-commercial-law">
			<?php if ( $custom_theme_commercial_law_field_featured_image ) : ?>
				<div class="fp-single-commercial-law__img" style="background-image: url('<?php echo esc_attr( $custom_theme_commercial_law_field_featured_image ); ?>')">
					<?php if ( $custom_theme_commercial_law_field_name ) : ?>
						<h1 class="text-white fp-single-commercial-law__title"> <?php echo esc_attr( $custom_theme_commercial_law_field_name ); ?> </h1>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<div class="fp-single-commercial-law-content">
				<div class="fp-single-commercial-law-content__left">
					<?php echo wp_kses_post( wpautop( $custom_theme_commercial_law_field_description ) ); ?>
					<?php if ( $custom_theme_commercial_law_field_posts ) : ?>
						<div>
							<ul class="fp-single-commercial-law__posts">
								<?php
								foreach ( $custom_theme_commercial_law_field_posts as $custom_theme_commercial_law_field_post ) :
									$custom_theme_commercial_law_field_post_permalink = get_permalink( $custom_theme_commercial_law_field_post->ID );
									$custom_theme_commercial_law_field_post_title     = get_the_title( $custom_theme_commercial_law_field_post->ID );
									?>
									<li class="fp-single-commercial-law__posts-item">
										<a class="fp-link" href="<?php echo esc_url( $custom_theme_commercial_law_field_post_permalink ); ?>">
											<img class="fp-link__arrow fp-link__arrow--orange" src="/frontend/src/icons/Arrow-icon.svg"/>
											<span class="fp-link__text text-darker-blue"><?php echo esc_attr( $custom_theme_commercial_law_field_post_title ); ?></span>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php endif; ?>
				</div>

				<div class="fp-single-commercial-law-content__right">
					<div class="fp-single-commercial-law-content__ask-law">
						<?php
						custom_theme_get_custom_block(
							'template-parts/blocks/ask-law/ask-law-mini'
						);
						?>
					</div>

					<?php
					custom_theme_get_custom_block(
						'template-parts/blocks/commercial-law/commercial-law-listing'
					);
					?>
				</div>
			</div>
		</article>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
custom_theme_get_custom_block(
	'template-parts/blocks/packages/packages'
);

get_footer();
