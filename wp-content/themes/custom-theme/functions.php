<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package custom-theme
 */

if ( ! function_exists( 'custom_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function custom_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on custom-theme, use a find and replace
		 * to change 'custom-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'custom-theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'custom-theme' ),
				'menu-2' => esc_html__( 'Footer', 'custom-theme' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'custom_theme_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'custom_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function custom_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'custom_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'custom_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function custom_theme_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'custom-theme' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'custom-theme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'custom_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function custom_theme_scripts() {

	if ( defined( 'RT_VUE_DEV' ) && RT_VUE_DEV ) {
		wp_enqueue_script( 'custom-theme-scripts', 'http://localhost:3000/main-bundle.js', array( 'jquery', 'acf-input', 'wp-i18n' ), '1.0.0', true );
		wp_enqueue_style( 'custom-theme-style', 'http://localhost:3000/css/styles.css', false, '1.0.0' );
	} else {
		wp_enqueue_script( 'custom-theme-scripts', get_template_directory_uri() . '/dist/main-bundle.js', array( 'jquery', 'custom-theme-vue', 'acf-input', 'wp-i18n' ), filemtime( get_template_directory() . '/dist/main-bundle.js' ), false );
		wp_enqueue_style( 'custom-theme-style', get_template_directory_uri() . '/dist/css/styles.css', false, filemtime( get_template_directory() . '/dist/css/styles.css' ) );
	}

	wp_enqueue_script( 'custom-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'custom-theme-order', get_template_directory_uri() . '/template-parts/blocks/order/order.js', array(), '20151215', true );

	wp_enqueue_script( 'custom-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'custom-theme-vue', 'https://cdn.jsdelivr.net/npm/vue@2.6.11', array(), '20200528', true );

	wp_enqueue_script( 'custom-theme-vue-router', 'https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.2.0/vue-router.js', array( 'custom-theme-vue' ), '20200528', true );

	wp_enqueue_style( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '1.8.1' );
	wp_enqueue_style( 'slick-theme', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css', array(), '1.8.1' );
	wp_enqueue_script( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( 'jquery' ), '1.8.1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'custom_theme_scripts' );

/**
 * Enqueue admin scripts and styles.
 */
function custom_theme_block_editor_styles() {
	if ( defined( 'RT_VUE_DEV' ) && RT_VUE_DEV ) {
		wp_enqueue_script( 'custom-theme-scripts', 'http://localhost:3000/main-bundle.js', array( 'jquery' ), '1.0.0', true );
		wp_enqueue_style( 'custom-theme-admin-styles', 'http://localhost:3000/css/styles.css', false, '1.0.0' );
	} else {
		wp_enqueue_script( 'custom-theme-scripts', get_template_directory_uri() . '/dist/main-bundle.js', array( 'jquery' ), filemtime( get_template_directory() . '/dist/main-bundle.js' ), true );
		wp_enqueue_style( 'custom-theme-admin-styles', get_template_directory_uri() . '/dist/css/styles.css', false, filemtime( get_template_directory() . '/dist/css/styles.css' ) );
	}
}
add_action( 'wp_enqueue_scripts', 'custom_theme_block_editor_styles' );
add_action( 'enqueue_block_editor_assets', 'custom_theme_block_editor_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * For security reasons don't show what version of wp this site is running on
 */
remove_action( 'wp_head', 'wp_generator' );

/**
 * Gutenberg blocks.
 */
require get_template_directory() . '/inc/blocks.php';

/**
 * Acf add options page
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( 'Våra kundcase Arkivsida' );
	acf_add_options_page( 'Paket' );
	acf_add_options_page( 'Block' );
	acf_add_options_page(
		array(
			'page_title' => 'Temainställningar',
			'menu_title' => 'Temainställningar',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
	);
}

/**
 * Register post types.
 */
require get_template_directory() . '/inc/post-types/cpt.php';

/**
 * Get custom template
 *
 * @param string $file File file.
 * @param string $category_slug Category_slug category_slug.
 */
function custom_theme_get_custom_template( $file = '', $category_slug = '' ) {
	include locate_template( $file . '.php', false, false );
}

/**
 * Get custom block
 *
 * @param string $file File file.
 * @param bool   $option option option.
 */
function custom_theme_get_custom_block( $file = '', $option = false ) {
	include locate_template( $file . '.php', false, false );
}

/**
 * Disabling gutenberg editor for some post types.
 *
 * @param string $can_edit important i guess.
 * @param object $post the post type.
 */
function custom_theme_disable_gutenberg( $can_edit, $post ) {
	$disable = array( 'post', 'digital-services', 'commercial-law' );
	if ( in_array( $post->post_type, $disable, true ) ) {
		return false;
	}
	return $can_edit;
};
add_filter( 'use_block_editor_for_post', 'custom_theme_disable_gutenberg', 10, 2 );

/**
 * Include API.
 */
require get_template_directory() . '/inc/smtp.php';
require get_template_directory() . '/inc/api.php';

/**
 * Render shortcodes.
 *
 * @param mixed  $value the field value.
 * @param string $post_id the post ID where the value is saved.
 * @param array  $field the field array containing all settings.
 */
function custom_theme_format_value( $value, $post_id, $field ) {
	return do_shortcode( $value );
}
add_filter( 'acf/format_value/name=contactform', 'custom_theme_format_value', 10, 3 );
add_filter( 'acf/format_value/name=footer_email_form', 'custom_theme_format_value', 10, 3 );

/**
 * Write out pricing of bundles in navigation.
 *
 * @param array $items The items.
 * @param array $args The args.
 */
function custom_theme_wp_nav_menu_objects( $items, $args ) {
	foreach ( $items as &$item ) {
		if ( get_field( 'show_bundles', $item ) ) {
			// Get right pricing.
			if ( have_rows( 'bundles', 'option' ) ) :
				while ( have_rows( 'bundles', 'option' ) ) :
					the_row();
					if ( get_sub_field( 'name' ) === $item->title ) :
						$item->title .= ' <span>' . get_sub_field( 'price' ) . ' kr/mån </span>';
					endif;
				endwhile;
			endif;
		}
	}

	return $items;
}
add_filter( 'wp_nav_menu_objects', 'custom_theme_wp_nav_menu_objects', 10, 2 );

/** Breadcrumbs */
add_theme_support( 'yoast-seo-breadcrumbs' );

/**
 * Correct breadcrumbs.
 *
 * @param array $links Array with links data.
 */
function custom_theme_yoast_breadcrumb_trail( $links ) {
	$post_type = get_post_type( get_the_ID() );

	if ( 'digital-services' === $post_type ) {
		$breadcrumb[] = array(
			'text' => 'Digitala tjänster',
			'url'  => '/digitala-tjanster/',
		);
		array_splice( $links, 1, -3, $breadcrumb );
	}

	if ( 'commercial-law' === $post_type ) {
		$breadcrumb[] = array(
			'text' => 'Affärsjuridik',
			'url'  => '/affarsjuridik/',
		);
		array_splice( $links, 1, -3, $breadcrumb );
	}

	return $links;
}
add_filter( 'wpseo_breadcrumb_links', 'custom_theme_yoast_breadcrumb_trail' );

/**
 * Allow SVG.
 *
 * @param array  $data Data.
 * @param string $file File.
 * @param string $filename Filename.
 * @param array  $mimes Mimes.
 */
function custom_theme_allow_svg( $data, $file, $filename, $mimes ) {
	global $wp_version;
	if ( '5.7.2' !== $wp_version ) {
		return $data;
	}

	$filetype = wp_check_filetype( $filename, $mimes );

	return array(
		'ext'             => $filetype['ext'],
		'type'            => $filetype['type'],
		'proper_filename' => $data['proper_filename'],
	);
}

add_filter( 'wp_check_filetype_and_ext', 'custom_theme_allow_svg', 10, 4 );

/**
 * CC mime types.
 *
 * @param array $mimes Mimes.
 */
function custom_theme_cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'custom_theme_cc_mime_types' );

/**
 * Fix SVG.
 */
function custom_theme_fix_svg() {
	echo '<style type="text/css">
		  	.attachment-266x266, .thumbnail img {
			   width: 100% !important;
			   height: auto !important;
		  	}
		</style>';
}
add_action( 'admin_head', 'custom_theme_fix_svg' );

/**
 * Makes first return from get_row_index() return 0 instead of 1.
 */
add_filter('acf/settings/row_index_offset', '__return_zero');