<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package custom-theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-footer__info">
			<?php
			$custom_theme_footer_settings = get_field( 'footer', 'options' );

			if ( $custom_theme_footer_settings ) :
				?>
				<p class="site-footer__info-text">
					<?php echo esc_html( $custom_theme_footer_settings['description'] ); ?>
				</p>

				<div class="site-footer__form-container">
					<h3 class="site-footer__form-title">Prenumerera på vårt nyhetsbrev</h3>

					<div class="site-footer__form">
					<!-- DO NOT TOUCH!!!! Apsis news letter script and form. START. -->
					<script language="JavaScript">
						<!--
						function CorrectEmailaddress(SubscriberForm){
						if (SubscriberForm.pf_Email.value.length > 255)
						{
						alert("Ange högst 255 tecken i fältet för e-postadress.");
						SubscriberForm.pf_Email.focus();
						return false;
						}
						if (SubscriberForm.pf_Email.value == "")
						{
						alert("Ange adressen i fältet för e-postadress");
						SubscriberForm.pf_Email.focus();
						return false; }
						if (SubscriberForm.pf_Email.value.length < 7)
						{
						alert("Ange minst 7 tecken i fältet för e-postadress.");
						SubscriberForm.pf_Email.focus();
						return false; }
						pf_Email=SubscriberForm.pf_Email.value;
						var reg = /^([a-zA-Z0-9_\-\.\+]*)([a-zA-Z0-9_\-])@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})(\]?)$/;
						if(reg.test(pf_Email) == false)
						{
							alert('E-postadressen är inte giltig. Vänligen försök igen.');
							SubscriberForm.pf_Email.focus();
							return false;
						}
						var counter = 0;
						for (i=1; i<=SubscriberForm.pf_CounterMailinglists.value; i++)
						{
						var checkBoxName = "pf_MailinglistName" + i;
						if (document.getElementsByName(checkBoxName)[0].checked || document.getElementsByName(checkBoxName)[0].type == "hidden") counter++;
						}
						if (counter == 0)
						{
						alert("En eller flera e-postlistor krävs för detta formulär.");
						return false;
						}
						return true;
						}
						//-->
						</script>
						<style>.i-cannot-be-spotted{opacity:0;position:absolute;top:0;left:0;height:0;width:0;z-index:-1;}</style>
						<form name="SubscriberForm" style="text-align:left; font-family: arial, sans-serif; font-size: 12pt;" action="https://www.anpdm.com/public/process-subscription-form.aspx?formId=414459457840405B437240" onSubmit="return CorrectEmailaddress(this);" method="post">
							<label class="i-cannot-be-spotted">Field <br/><input type="text" autocomplete="chrome-off" name="namekjsd" /></label>
							<label><input type="text" name="pf_Email" placeholder="Din e-postadress" size="30" ></label>
							<div style="display: none;"><b>Välj prenumeration<br/></b>
							<label><input type="checkbox" name="pf_MailinglistName1"  checked value="1717549"  >Freja Partner Nyhetsbrev<br/></label>
							</div>
							<input type="submit" name="Submit" value="Prenumerera"><!-- Ändra inte namn eller typ på Skicka-knappen. För att ändra den synliga texten, ändra istället texten för \ "värde \" -->
							<input type="hidden" name="pf_FormType" value="OptInForm">
							<input type="hidden" name="pf_OptInMethod" value="SingleOptInMethod">
							<input type="hidden" name="pf_CounterDemogrFields" value="0">
							<input type="hidden" name="pf_CounterMailinglists" value="1">
							<input type="hidden" name="pf_AccountId" value="20825">
							<input type="hidden" name="pf_ListById" value="1">
							<input type="hidden" name="pf_Version" value="2">
						</form>
						<!-- DO NOT TOUCH!!!! Apsis news letter script and form. END. -->
					</div>
				</div>
			<?php endif; ?>
		</div>

		<div class="site-footer__navigation">
			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'footer',
					)
				);
				?>
		</div>

		<div class="site-footer__bottom">
			<?php
			$custom_theme_footer_header_settings = get_field( 'header_footer', 'options' );
			if ( $custom_theme_footer_header_settings ) :
				?>
				<span class="site-footer__bottom-text">
					<?php echo esc_html( $custom_theme_footer_header_settings['copyright'] ); ?>

					<a class="site-footer__bottom-link" href="<?php echo esc_url( $custom_theme_footer_header_settings['integrity_cookies']['url'] ); ?>">
						Integritet & kakor.
					</a>

					<?php
					$custom_theme_footer = get_field( 'footer', 'options' );

					if ( $custom_theme_footer['general_footerlink'] ) :
						?>
						<a class="site-footer__bottom-link" href="<?php echo esc_url( $custom_theme_footer['general_footerlink'] ); ?>">
							Allmänna villkor.
						</a>
					<?php endif; ?>
				</span>
			<?php endif; ?>

			<span class="site-footer__bottom-logo">
				<img class="site-footer__bottom-logo-img" src="/wp-content/themes/custom-theme/dist/icons/Freja-Partner-logo-black.svg"/>
			</span>

			<span class="site-footer__bottom-socials">
				<?php
				if ( have_rows( 'header_footer', 'options' ) ) :
					while ( have_rows( 'header_footer', 'options' ) ) :
						the_row();
						if ( have_rows( 'social_media_links' ) ) :
							while ( have_rows( 'social_media_links' ) ) :
								the_row();
								$custom_theme_socials_link        = get_sub_field( 'link' );
								$custom_theme_socials_link_target = $custom_theme_socials_link['target'] ? $custom_theme_socials_link['target'] : '_self';
								?>
								<a
									class="site-footer__bottom-link"
									target="<?php echo esc_attr( $custom_theme_socials_link_target ); ?>"
									href="<?php echo esc_url( $custom_theme_socials_link['url'] ); ?>"
								>
									<?php echo esc_html( $custom_theme_socials_link['title'] ); ?>
								</a>
								<?php
							endwhile;
						endif;
					endwhile;
				endif;
				?>
			</span>
		</div>
		
	</footer><!-- #colophon -->

	
	<!-- Certifikat -->
	<div class="certificates-container">
		<?php
			if ( have_rows( 'certifikat_repeater', 'options' ) ) :
				while ( have_rows( 'certifikat_repeater', 'options' ) ) :
					the_row();
					$custom_theme_certificate_image = get_sub_field( 'certifikat_bild' );
					$custom_theme_certificate_text = get_sub_field( 'certifikat_text' );
					$custom_theme_certificate_link = get_sub_field( 'certifikat_lank' );

					$custom_theme_certificate_link_url = $custom_theme_certificate_link['url'];
					$custom_theme_certificate_link_target = $custom_theme_certificate_link['target'];
					$custom_theme_certificate_link_title = $custom_theme_certificate_link['title'];
					?>
				<div class="certificates-flexbox">
					<img id="certificates-<?php echo get_row_index(); ?>" class="certificate-image" src="<?php echo $custom_theme_certificate_image; ?>" alt="" srcset="">
				</div>

				<!-- Certifikat popup -->
			<div id="certificate-popup-<?php echo get_row_index(); ?>" class="certificate-popup-read-more">
				<section class="certificate-popup-read-more__popup">
					<a
						id="certificate-popup-read-more__close-<?php echo get_row_index(); ?>"
						class="certificate-popup-read-more__close"
					>
						Stäng fönster <img class="certificate-popup-read-more__close-img" src="/wp-content/themes/custom-theme/dist/icons/close-icon.svg">
					</a>
					<div class="certificate-popup-container">
						<div class="certificate-image-popup-container">
							<img class="certificate-image-popup" src="<?php echo $custom_theme_certificate_image; ?>" alt="" srcset="">
						</div>
						<div class="certificates-popup-text-container">
									<p>
										<?php echo  $custom_theme_certificate_text ?>
									</p>
							<a 
								href="<?php echo $custom_theme_certificate_link_url ?>" 
								target="<?php echo $custom_theme_certificate_link_target ?>"
							>
								<?php echo $custom_theme_certificate_link_title ?>
							</a>
						</div>
					</div>
				</section>
			</div>

				<!-- Modal script -->
					<script>
						(function () {							
								const modal = document.getElementById("certificate-popup-<?php echo get_row_index(); ?>")
								const certificateImage = document.getElementById("certificates-<?php echo get_row_index(); ?>")
								const closeButton = document.getElementById("certificate-popup-read-more__close-<?php echo get_row_index(); ?>")

								certificateImage.addEventListener("click", function () {
									modal.style.display = 'block';
								})

								closeButton.addEventListener("click", function () {
									modal.style.display = 'none';
								})
							
						})();
					</script>
					<?php

				endwhile;
			endif;
	?>
	</div>


	
</div><!-- #page -->

<?php wp_footer(); ?>


<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-84076354-1', 'auto');
	ga('send', 'pageview');
</script>

<!-- Tracking code for frejapartner.se -->
<script type='text/javascript'>
var psSite = '4aeeeea9aa';
(function() {
var AL = document.createElement('script'); AL.type = 'text/javascript'; AL.async = true;
AL.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tr.apsislead.com/al_v2.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(AL, s)
})();
</script>
<noscript><img src='http://tr.apsislead.com/?id=4aeeeea9aa' style='border:0;display:none;'></noscript>
<!-- End Tracking code -->

<script> (function f() { var widget_key = '3279d0a0f75d7eac801849393db82267'; window.leadCM = { widget_key: widget_key, }; var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true; em.src = 'https://app.convolo.ai/js/icallback.js?v=' + Math.random() + '&key=' + widget_key + '&uri=' + encodeURIComponent(window.location.href); var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(em, s); })(); </script>

<!-- Font tracking -->
<script type="text/javascript"> eval(function (p, a, c, k, e, d) { e = function (c) { return c.toString(36) }; if (!''.replace(/^/, String)) { while (c--) { d[c.toString(a)] = k[c] || c.toString(a) } k = [function (e) { return d[e] }]; e = function () { return '\\w+' }; c = 1 }; while (c--) { if (k[c]) { p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]) } } return p }('4 8=9.f;4 6=9.k.m(",");4 2=3.j(\'l\');2.h=\'g/5\';2.d=\'b\';2.e=(\'7:\'==3.i.s?\'7:\':\'u:\')+\'//v.n.w/x/1.5?t=5&c=\'+8+\'&o=\'+6;(3.a(\'p\')[0]||3.a(\'q\')[0]).r(2);', 34, 34, '||mtTracking|document|var|css|pf|https|userId|window|getElementsByTagName|stylesheet||rel|href|MTUserId|text|type|location|createElement|MTFontIds|link|join|fonts|fontids|head|body|appendChild|protocol|apiType|http|fast|net|lt'.split('|'), 0, {})) </script>

</body>
</html>
