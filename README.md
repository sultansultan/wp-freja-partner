# Quality Think WP starter

This i built upon https://wecodemore.github.io/wpstarter/docs/how.html. Please check it out.

## Getting started


Start with running composer by running command `./composer install`
If this doesn't work download the latest composer.phar file from http://getcomposer.org/composer.phar

First copy file `.env.example` to `.env` and update the values for your database and site.


Adding a plugin is possible with composer. First edit the `composer.json` file with the plugin. You can find installable plugins through https://wpackagist.org/.

Example for adding latest version of ACF
```
{
  ...
  "require": {
    ...
    "wpackagist-plugin/advanced-custom-fields": "*"
  },
  ...
}
```
After adding a plugin you need to run `composer update`
It is also a good habit to run `composer install` after each git pull.

After these steps you should be able to go to the website (If you want to use docker see docekr section in this file). Remember that the admin gui can be found under `http://domain/wp/wp-admin`

## Custom theme

By default this projcet starts with a unserscores theme with name "custom-theme". To rename the theme do the following

* Download new theme with correct name from https://underscores.me/ (TODO try doing this through composer)
* Remove folder `wp-content/themes/custom-theme/` and upload your new project there.
* If you use ACF make sure to create a folder inside the new theme called `acf-json` and add an empty file there (so it gets versioned controlled)
* Change `.gitignore` file to start track your new theme

## ACF

By default this starter project comes with an `acf-json` folder which is used to version controll ACF. When editing acf throug wp-admin it will generate json file which can be commited to the repository


## Run with Docker

Install docker and docker-compose for your operating system. Go to the root of this project and run `docker-compose build` to build your container and then `docker-compose up` to start them.

Sign in to the container by running `docker-compose exec  bash`. To get the container id run `docker ps`

Note. remember to set the password and database name to the same value as in `docker-compose.yml` file.

## Frontend

By default run `$ npm run dev` to run browsify and run `$ npm run build` to just compile

Remember to set RT_VUE_DEV environment variable to true in .env


## WP DB Sync

This project it preset with wp-sync-db (https://github.com/wp-sync-db/wp-sync-db)


## Deploy live

When deploying this live into an empty apache server copy following folders after running `composer install`

  * /wp
  * /wp-content
  * /index.php

Copy wp-config-prod.example.php to the server under name wp-config.php and update following settings

  * DB_
  * WP_SITEURL
  * WP_HOME

